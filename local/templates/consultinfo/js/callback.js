/*===========================
	CallBack Functions
	------------------

	List:
	-------------------------
	[0] - website preloader
	[1] - Slimmenu
	[2] - Testimonial carousel
	[3] - Show/hide header on scroll top/down
	[4] - Services tab list
	[5] - Animations + counters
	[6] - Scroll top button [ DOCUMENT SCROLL ]
	[7] - Scroll to...
	[8] - Show full block
	[9] - Show block in section.about
	[10] - Horizontal parallax
	[11] - Feedback validation
	[12] - Subscription Popup
	[13] - App accordion
	[14] - Dropdown
	[15] - show/hide prompt on app page
	[16] - show/hide doc list
	[17] - show/hide testimonials
	[18] - show/hide service's description
	[19] - show/hide terms's subscription
	[19] - show/hide terms's subscription

===========================*/


/****** DOCUMENT READY ******/

$(document).ready(function(){

	/*------[0]------*/

//	$("#preloader").fakeLoader({
//		timeToHide:2500, //Time in milliseconds for fakeLoader disappear
//		zIndex:"9999",//Default zIndex
//		spinner:"spinner2",//Options: 'spinner1', 'spinner2', 'spinner3', 'spinner4', 'spinner5', 'spinner6', 'spinner7'
//		bgColor:"#C4D8E1", //Hex, RGB or RGBA colors
//		//imagePath:"bitrix/templates/sample/img/etc/preloader.gif" //If you want you can insert your custom image
//	});

	/*------[1]------*/

	$('ul.topNav').slimmenu({
	    resizeWidth: '1199',
	    collapserTitle: '',
	    easingEffect:'easeInOutQuint',
	    animSpeed:'medium',
	    indentChildren: true,
	    childrenIndenter: ''
	});


	/*$('ul.topNav.collapsed').on('click','li > a',function(){
		$(this).parent().find('.sub-collapser').trigger('click');
	});*/

	$('.sub-collapser').hover(function(){
		$(this).prev().prev().toggleClass('selected');
	});

	/*------[2]------*/

	$('.testimonials').owlCarousel({
	    loop: true,
	    nav:true,
	    margin: 3,
	    dots: false,
	    navText: [ '' ,'' ],
	    responsiveClass: true,
	    responsive:{
	        0:{
	            items:1
	        },
	        321:{
	            items:1
	        },
	        481:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        767:{
	            items:2
	        },
	        991:{
	            items:3
	        },
	        1200:{
	            items:3
	        },
	        1450:{
	            items:3
	        },
	        1451:{
	            items:3
	        }
	    }
	});

	/*------[3]------*/

	// var mywindow = $(window);
	// var mypos = mywindow.scrollTop();
	// var up = false;
	// var newscroll;
	// mywindow.scroll(function () {
	//     newscroll = mywindow.scrollTop();
	//     if (newscroll > mypos && !up) {
	//         $('header.header').stop().addClass( 'scroll' );
	//         up = !up;
	//     } else if(newscroll < mypos && up) {
	//         $('header.header').stop().removeClass( 'scroll' );
	//         up = !up;
	//     }
	//     mypos = newscroll;
	// });

	/*------[4]------*/

	$( "#services" ).tabs().addClass( "ui-tabs-vertical clearFix" );

	/*------[5]------*/

	if (jQuery().waypoint){
        jQuery('.animate_afc, .animate_afl, .animate_afr, .animate_aft, .animate_afb, .animate_wfc, .animate_hfc, .animate_rfc, .animate_rfl, .animate_rfr').waypoint(function() {
            if ( ! jQuery(this).hasClass('animate_start')){
                var elm = jQuery(this);
                setTimeout(function() {
                    elm.addClass('animate_start');
                }, 20);
            }
        }, {offset:'85%', triggerOnce: true});

        jQuery('.counter').waypoint(function() {
            var counter = jQuery(this).find('span.value'),
                count = parseInt(counter.text(), 10),
                prefix = '',
                suffix = '',
                number = 0;

            if (jQuery(this).data('count')) {
                count = parseInt(jQuery(this).data('count'), 10);
            }
            if (jQuery(this).data('prefix')) {
                prefix = jQuery(this).data('prefix');
            }
            if (jQuery(this).data('suffix')) {
                suffix = jQuery(this).data('suffix');
            }

            var	step = Math.ceil(count/25),
                handler = setInterval(function() {
                    number += step;
                    counter.text(prefix+number+suffix);
                    if (number >= count) {
                        counter.text(prefix+count+suffix);
                        window.clearInterval(handler);
                    }
                }, 40);


        }, {offset:'85%', triggerOnce: true});
	}

	/*------[7]------*/

	$("a.link, #backToTop a").click(function(evn){
        evn.preventDefault();
        $('html,body').scrollTo(this.hash, this.hash);
    });

    /*------[8]------*/

    var inner = $( 'section.about .featuresWrapper section.inner' );
    var trigger = $( 'section.about a.launcher' );

    $( trigger ).click(function(){
    	var id = $(this).data("id");
    	//var header = $("section.about header.feature").height();
    	$("#block_"+id).toggleClass( 'expanded', 1000,  'easeOutSine' );

//    	$("#block_"+id).animate({
//    		height: $(this).find("section.description").height() + header
//    	}, {
//    		duration: 150
//    	});


    });

    /*------[9]------*/

    $( 'section.whatIs' ).click(function(){
    	$( 'section.whatIs' ).toggleClass( 'visible' );
    });

    /*------[10]------*/

    $('#parallax').parallax('20%', '-0.02');

    /*------[11]------*/

    $('#feed').bind('submit', function(event) {
		$('#feed input,#feed textarea').each(function() {
			if(!$(this).val().length) {
			  event.preventDefault();
			  $(this).addClass('error');
			}
			else {
				$('#feed input,#feed textarea').keyup(function() {
					$(this).removeClass('error');
				});
			}
		});
	});

    /*------[12]------*/

	$('.sTrigger').bind('click', function(e) {
		e.preventDefault();
		$('#subscribe').bPopup({
			speed: 250,
			transition: 'slideUp'
		});
	});

	$('div.app .appWrap .unit .value a.btn').bind('click', function(e) {
		e.preventDefault();
		$('#address').bPopup({
			speed: 250,
			transition: 'slideUp'
		});
	});

	/*$('section.half .item a.btn').bind('click', function(e) {
		e.preventDefault();
		$('#request').bPopup({
			speed: 250,
			transition: 'slideUp'
		});
	});*/

	/*------[13]------*/

	$('.app-form [data-accordion]').accordion({
    	singleOpen: true
    });

	/*------[14]------*/

    $('section.app-form article.wrapper .unit select').styler();

    /*------[15]------*/

    /*var unit = $( 'section.app-form article.wrapper .unit .value' );
    var item = $( 'section.app-form article.wrapper .part .item .value' );

    $( unit ).hover(function(){
    	$(this).children('span.prompt').toggleClass('active');
    });
    $( item ).hover(function(){
    	$(this).children('span.prompt').toggleClass('active');
    });*/

    /*------[16]------*/

    var parent = $('section.samples .unit .aritem');
    var link = $('section.samples .unit .aritem .link a.trigger');
    var list = $('section.samples .unit .aritem .arList');

    $( parent ).find( link ).click(function(){
    	$( this ).toggleClass( 'selected' );
    	if ( $( this ).hasClass( 'selected' ) ) {
    		$( this ).parent().parent().find( list ).slideToggle(300);
    	}
    	else {
    		$( this ).parent().parent().find( list ).slideToggle(300);
    	}
    });

    /*------[17]------*/

    var tstmnl = $('.testimonials .item');
    var more = $('.testimonials .item a.more');
    var context = $('.testimonials .item .context');

    $( tstmnl ).find( more ).click(function(){
    	$(this).prev( context ).toggleClass( 'full' );
    	if ( $(this).prev( context ).hasClass( 'full' ) ) {
    		$(this).html( 'Cкрыть' );
    	}
    	else {
    		$(this).html( 'Читать весь отзыв' );
    	}
    });

    /*------[18]------*/

   	var srvc = $('section.serviceItem article.item');
    var wrp = $('section.serviceItem article.item section.description .inner .wrp');
    var s_more = $('section.serviceItem article.item section.description a.more');

    srvc.find( s_more ).click(function(){
    	$(this).prev().toggleClass( 'expanded' );
    	if ( $(this).prev().hasClass( 'expanded' ) ) {
    		$(this).html( 'Cкрыть' );
    	} else {
    		$(this).html( 'Читать полностью' );
    	}
    });

    wrp.each(function(){
    	if( $(this).height() > 187 ){
    		if ( $(this).parent().next().hasClass('more') ) {
    			$(this).parent().next().css('display', 'block');
    		}
    	} else {
    		if ( $(this).parent().next().hasClass('more') ) {
    			$(this).parent().next().css('display', 'none');
    		}
    	}
    });

    /*------[19]------*/

    $('a.terms_trigger').bind('click', function(e) {
		e.preventDefault();
		$('#terms').bPopup({
			speed: 250,
			transition: 'slideUp'
		});
	});

});


/****** DOCUMENT SCROLL ******/

	/*------[6]------*/

$(document).scroll(function(){
	if($(window).scrollTop() > 400){
		$("#backToTop").addClass( 'visible' );
	} else{
		$("#backToTop").removeClass( 'visible' );
	}
});

// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header.header').outerHeight();

$(window).scroll(function(event){
	didScroll = true;
});

setInterval(function() {
	if (didScroll) {
		hasScrolled();
		didScroll = false;
	}
}, 250);

function hasScrolled() {
	var st = $(this).scrollTop();

	if(Math.abs(lastScrollTop - st) <= delta)
		return;

	if (st > lastScrollTop && st > navbarHeight){
		$('header.header').addClass('scroll');
	} else {
		if(st + $(window).height() < $(document).height()) {
			$('header.header').removeClass('scroll');
		}
	}

	lastScrollTop = st;
}
