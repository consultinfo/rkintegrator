<?
    if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
    $this->setFrameMode(false);
    require(__DIR__ . '/../questionnaires_llc/CreateInput.php');
    $APPLICATION->AddHeadScript($this->GetFolder() . '/../questionnaires_llc/script.js');
?>
<div class="container light">
    <div class="static clearFix">
        <div class="app clearFix">
            <h4>
                Во время заполнения анкета сохраняется в черновике. Вы можете возобновить работу по ней в любой удобный
                момент, перейдя по ссылке из письма.
            </h4>
        </div>
    </div>
</div>
<div class="container">
    <div class="static">
        <div id="app">
            <div class="ctrls clearFix">
                <ul class="tabs triggers">
                    <li class="active">
                        <a class="block" data-target="#ip" href="javascript:void(0);">Анкета для ИП</a>
                    </li>
                </ul>
            </div>
        </div>
        <section class="app">
            <h5 class="none">application forms</h5>

            <div class="tabs-content">
                <form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
                    <div id="ip"
                         class="tabs-pane active">
                        <section class="app-form" data-accordion-group>
                            <h5 class="none">applicationn step</h5>
                            <?=bitrix_sessid_post()?>
                            <? if ($arParams["MAX_FILE_SIZE"] > 0) { ?>
                                <input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" />
                            <? } ?>
                            <!--<input type="hidden" value="Название" name="PROPERTY[NAME][0]">-->

                            <!--Базовая информация-->
                            <? if ($arResult['ELEMENT_PROPERTIES']['143'][0]['VALUE_ENUM'] == 'да') { ?>
                                <section class="step" data-accordion>
                                    <h5 class="none">application step</h5>
                                    <a data-control href="javascript:void(0);">Базовая информация</a>
                                    <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                    <div class="content" data-content>
                                        <article class="wrapper noselect">
                                            <h5 class="none">inner of the app step</h5>
                                            <? CreateInput(86, $arResult, $arParams) ?>
                                            <? CreateInput(87, $arResult, $arParams) ?>
                                            <? CreateInput(88, $arResult, $arParams) ?>
                                            <? CreateInput(89, $arResult, $arParams) ?>
                                            <? CreateInput(90, $arResult, $arParams) ?>
                                            <? CreateInput(91, $arResult, $arParams) ?>
                                            <? CreateInput(92, $arResult, $arParams) ?>
                                            <? CreateInput(93, $arResult, $arParams) ?>
                                            <div class="unit_wrapper" rel="property_29">
                                                <div class="checked">
                                                    <? CreateInput(94, $arResult, $arParams) ?>
                                                    <? CreateInput(95, $arResult, $arParams) ?>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </section>
                            <? } ?>
                            <!--/Базовая информация-->

                            <!--Реквизиты документа, удостоверяющего личность-->
                            <? if ($arResult['ELEMENT_PROPERTIES']['144'][0]['VALUE_ENUM'] == 'да') { ?>
                                <section class="step" data-accordion>
                                    <h5 class="none">application step</h5>
                                    <a data-control href="javascript:void(0);">
                                        Реквизиты документа, удостоверяющего личность
                                    </a>
                                    <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                    <div class="content" data-content>
                                        <article class="wrapper noselect">
                                            <h5 class="none">inner of the app step</h5>
                                            <? CreateInput(96, $arResult, $arParams) ?>
                                            <? CreateInput(97, $arResult, $arParams) ?>
                                            <? CreateInput(98, $arResult, $arParams) ?>
                                            <? CreateInput(99, $arResult, $arParams) ?>
                                            <? CreateInput(100, $arResult, $arParams) ?>
                                            <? CreateInput(101, $arResult, $arParams) ?>
                                        </article>
                                    </div>
                                </section>
                            <? } ?>
                            <!--/Реквизиты документа, удостоверяющего личность-->
                            <div class="unit_wrapper mb30" rel="property_29">
                                <div class="unchecked">
                                    <!--Данные миграционной карты-->
                                    <? if ($arResult['ELEMENT_PROPERTIES']['145'][0]['VALUE_ENUM'] == 'да') { ?>
                                        <?/*<div class="btnWrp">
                                            <a class="add" href="javascript:void(0);">
                                                <span>Указать</span> данные миграционной карты
                                            </a>
                                        </div>*/?>
                                        <section class="step"
                                                 data-accordion>
                                            <h5 class="none">application step</h5>
                                            <a data-control
                                               href="javascript:void(0);">Данные миграционной карты</a>
                                            <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                            <div class="content"
                                                 data-content>
                                                <article class="wrapper noselect">
                                                    <h5 class="none">inner of the app step</h5>
                                                    <? CreateInput(102, $arResult, $arParams) ?>
                                                    <? CreateInput(103, $arResult, $arParams) ?>
                                                    <? CreateInput(104, $arResult, $arParams) ?>
                                                    <? CreateInput(105, $arResult, $arParams) ?>
                                                </article>
                                            </div>
                                        </section>
                                    <? } ?>
                                    <!--/Данные миграционной карты-->

                                    <!--Данные документа, подтверждающего право иностранного гражданина или лица без гражданства на пребывание (проживание) в РФ-->
                                    <? if ($arResult['ELEMENT_PROPERTIES']['146'][0]['VALUE_ENUM'] == 'да') { ?>
                                        <?/*<div class="btnWrp">
                                            <a class="add" href="javascript:void(0);">
                                                <span>Указать</span> разрешение на пребывание (проживание) в РФ
                                            </a>
                                        </div>*/?>
                                        <section class="step" data-accordion>
                                            <h5 class="none">application step</h5>
                                            <a data-control href="javascript:void(0);">
                                                Данные документа, подтверждающего право иностранного гражданина или лица
                                                без
                                                гражданства на пребывание (проживание) в РФ
                                            </a>
                                            <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                            <div class="content" data-content>
                                                <article class="wrapper noselect">
                                                    <h5 class="none">inner of the app step</h5>
                                                    <? CreateInput(106, $arResult, $arParams) ?>
                                                    <? CreateInput(107, $arResult, $arParams) ?>
                                                    <? CreateInput(108, $arResult, $arParams) ?>
                                                    <? CreateInput(109, $arResult, $arParams) ?>
                                                    <? CreateInput(110, $arResult, $arParams) ?>
                                                </article>
                                            </div>
                                        </section>
                                    <? } ?>
                                    <!--/Данные документа, подтверждающего право иностранного гражданина или лица без гражданства на пребывание (проживание) в РФ-->
                                </div>
                            </div>
                            <!--Адреса физического лица-->
                            <? if ($arResult['ELEMENT_PROPERTIES']['147'][0]['VALUE_ENUM'] == 'да') { ?>
                                <section class="step" data-accordion>
                                    <h5 class="none">application step</h5>
                                    <a href="javascript:void(0);" data-control> Адреса физического лица </a>
                                    <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                    <div class="content" data-content>
                                        <article class="wrapper noselect">
                                            <h5 class="none">inner of the app step</h5>
                                            <? CreateInput(111, $arResult, $arParams) ?>
                                            <? CreateInput(112, $arResult, $arParams) ?>
                                            <? CreateInput(113, $arResult, $arParams) ?>
                                            <? // CreateInput(114, $arResult, $arParams) ?>
                                            <? CreateInput(115, $arResult, $arParams) ?>
                                        </article>
                                    </div>
                                </section>
                            <? } ?>
                            <!--/Адреса физического лица-->

                            <!--Cведения о государственной регистрации физического лица в качестве индивидуального предпринимателя-->
                            <? if ($arResult['ELEMENT_PROPERTIES']['148'][0]['VALUE_ENUM'] == 'да') { ?>
                                <section class="step" data-accordion>
                                    <h5 class="none">application step</h5>
                                    <a href="javascript:void(0);" data-control>
                                        Cведения о государственной регистрации физического лица в качестве
                                        индивидуального
                                        предпринимателя
                                    </a>
                                    <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                    <div class="content" data-content>
                                        <article class="wrapper noselect">
                                            <h5 class="none">inner of the app step</h5>
                                            <? CreateInput(116, $arResult, $arParams) ?>
                                            <? CreateInput(117, $arResult, $arParams) ?>
                                            <? CreateInput(175, $arResult, $arParams) ?>
                                            <? CreateInput(118, $arResult, $arParams) ?>
                                            <? CreateInput(119, $arResult, $arParams) ?>
                                            <? CreateInput(120, $arResult, $arParams) ?>
                                            <? CreateInput(121, $arResult, $arParams) ?>
                                            <? // CreateInput(122, $arResult, $arParams) ?>
                                        </article>
                                    </div>
                                </section>
                            <? } ?>
                            <!--/Cведения о государственной регистрации физического лица в качестве индивидуального предпринимателя-->

                            <!--Реквизиты банковского счета в рублях-->
                            <? if ($arResult['ELEMENT_PROPERTIES']['149'][0]['VALUE_ENUM'] == 'да') { ?>
                                <section class="step" data-accordion>
                                    <h5 class="none">application step</h5>
                                    <a href="javascript:void(0);" data-control>Реквизиты банковского счета в рублях</a>
                                    <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                    <div class="content" data-content>
                                        <article class="wrapper noselect">
                                            <h5 class="none">inner of the app step</h5>
                                            <? CreateCompositeInputs(127, $arResult, $arParams, 'Банковский счет',
                                                                     array(
                                                                         'БИК'                                   => array(
                                                                             true,
                                                                             ''
                                                                         ),
                                                                         'Наименование банка с указанием города' => array(
                                                                             true,
                                                                             ''
                                                                         ),
                                                                         'К/с'                                   => array(
                                                                             true,
                                                                             ''
                                                                         ),
                                                                         'Номер расчетного счета'                => array(
                                                                             true,
                                                                             ''
                                                                         ),
                                                                     )
                                            ) ?>
                                            <? /* CreateCompositeInputs(127, $arResult, $arParams, 'Банковский счет',
                                                                 array(
                                                                     'Наименование банка с указанием города',
                                                                     'БИК',
                                                                     'К/С',
                                                                     'Р/С',
                                                                     'Л/С',
                                                                     'КАРТ/С'
                                                                 )
                                        ) */ ?>
                                            <a class="add" href="javascript:void(0);">Добавить еще один банковский
                                                счет</a>
                                        </article>
                                    </div>
                                </section>
                            <? } ?>
                            <!--/Реквизиты банковского счета в рублях-->

                            <!--Уполномоченные лица-->
                            <? if ($arResult['ELEMENT_PROPERTIES']['150'][0]['VALUE_ENUM'] == 'да') { ?>
                                <div class="btnWrp">
                                    <a class="add" href="javascript:void(0);"><span>Указать</span>представителей по доверенности</a>
                                </div>
                                <section class="step" data-accordion>
                                    <h5 class="none">application step</h5>
                                    <a href="javascript:void(0);" data-control>Представители по доверенности</a>
                                    <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                    <div class="content" data-content>
                                        <article class="wrapper noselect">
                                            <h5 class="none">inner of the app step</h5>
                                            <? CreateCompositeInputs(123, $arResult, $arParams, 'Банковский счет',
                                                                     array(
                                                                         'Фамилия'                   => array(true, ''),
                                                                         'Имя'                       => array(true, ''),
                                                                         'Отчество'                  => array(true, ''),
                                                                         'Должность'                 => array(true, ''),
                                                                         'Номер доверенности'        => array(true, ''),
                                                                         'Дата доверенности'         => array(
                                                                             true,
                                                                             'date'
                                                                         ),
                                                                         'Дата начала полномочий'    => array(
                                                                             true,
                                                                             'date'
                                                                         ),
                                                                         'Дата окончания полномочий' => array(
                                                                             true,
                                                                             'date'
                                                                         ),
                                                                     )
                                            ) ?>
                                            <a class="add" href="javascript:void(0);">Добавить представителя</a>
                                        </article>
                                    </div>
                                </section>
                            <? } ?>
                            <!--/Уполномоченные лица-->

                            <section class="laststep">
                                <h5 class="bold">Анкету заполнил</h5>
                                <span class="progress" style="display: none">Заполнено <span>0</span>%</span>
                                <? CreateInput(124, $arResult, $arParams) ?>
                                <? CreateInput(125, $arResult, $arParams) ?>
                                <? CreateInput(126, $arResult, $arParams) ?>
                            </section>

                            <div class="notice center">
                                Все изменения анкеты сохранены<br>Заполните все необходимые поля для отправки
                            </div>

                            <div class="center noselect">
                                <input type="submit" name="iblock_submit" value="Отправить анкету"
                                       disabled="disabled" />
                            </div>
                        </section>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>