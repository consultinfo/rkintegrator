/*Проверка правильности поля*/
function ValidateInput(input){
    var rel = input.attr('rel');
    var name = input.attr('name');
    var result = '';
    if(input.parents('.llc').length == 1){
        if(rel == 'p_25_4')
            result = is_valid_inn(input);
        if(rel == 'p_25_3')
            result = is_valid_ogrn(input);
    }




    if(rel == 'p_133' || rel == 'p_92')
        result = is_valid_inn(input);
    else if(rel == 'p_7')
        result = is_valid_ogrn(input);
    else if(rel == 'p_11')
        result = is_valid_kpp(input);
    else if(rel == 'p_12')
        result = is_valid_okpo(input);
    else if(rel == 'p_47_1')
        result = is_valid_bank_bik(input);
    else if(rel == 'p_47_4')
        result = is_valid_bank_rs(input);
    else if(rel == 'p_47_3')
        result = is_valid_bank_cs(input);
    else if(rel == 'p_24' || rel == 'p_58' || rel == 'p_115')
        result = is_valid_email(input);
    else if(rel == 'p_13')
        result = is_valid_oktmo(input);
    else if(rel == 'p_17')
        result = is_valid_okato(input);
    else if(rel == 'p_18')
        result = is_valid_okopf(input);
    else if(rel == 'p_16')
        result = is_valid_okfs(input);
    else if(rel == 'p_15')
        result = is_valid_okogu(input);
    //else if(rel == 'p_14')
    //    result = is_valid_okved(input);


    if(result != ''){
        var prompt = '<span class="prompt active"><span class="inner">'+ result +'</span></span>';
        input.parents('.unit').addClass('error');
        input.after(prompt);
        return false;
    }

    return true;

}
/*Проверка правильности поля*/


/* Проверка email */
function is_valid_email(email) {
    var regex = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
    if(regex.test(email.val())) return '';
    else return 'Некорректный адрес E-mail';
}
/* Проверка email */


/* Проверка ИНН */
function is_valid_inn(i) {
    var val = i.val();
    if (val.match(/\D/)) return 'Неверный формат номера';
    if (val.length != 12 && val.length != 10) return 'Неверный формат номера';
    var inn = val.match(/(\d)/g);
    if (inn.length == 10) {
        if(inn[9] == String(((
                    2 * inn[0] + 4 * inn[1] + 10 * inn[2] +
                    3 * inn[3] + 5 * inn[4] + 9 * inn[5] +
                    4 * inn[6] + 6 * inn[7] + 8 * inn[8]
                ) % 11) % 10)) {
            return '';
        } else {
            return 'Проверьте правильность номера';
        }
    } else if (inn.length == 12) {
        if(inn[10] == String(((
                    7 * inn[0] + 2 * inn[1] + 4 * inn[2] +
                    10 * inn[3] + 3 * inn[4] + 5 * inn[5] +
                    9 * inn[6] + 4 * inn[7] + 6 * inn[8] +
                    8 * inn[9]
                ) % 11) % 10) && inn[11] == String(((
                    3 * inn[0] + 7 * inn[1] + 2 * inn[2] +
                    4 * inn[3] + 10 * inn[4] + 3 * inn[5] +
                    5 * inn[6] + 9 * inn[7] + 4 * inn[8] +
                    6 * inn[9] + 8 * inn[10]
                ) % 11) % 10)){
            return '';
        } else {
            return 'Проверьте правильность номера';
        }
    }
}
/* Проверка ИНН */


/* Проверка ОГРН */
function is_valid_ogrn(i) {
    var val = i.val();
    if (val.match(/\D/)) return 'Неверный формат номера';
    if (val.length != 13 && val.length != 15) return 'Неверный формат номера';
    // для ЮЛ
    if (val.length == 13) {
        // проверка по контрольным цифрам
        var num12 = val;
        num12 = Math.floor((num12 / 10) % 11);
        if (num12 == 10) {
            dgt13 = 0;
        }
        else {
            dgt13 = num12;
        }
        if (val[12] == dgt13) {
            return '';
        } else {
            return 'Проверьте правильность номера';
        }
    }
    // ИП
    if (val.length == 15) {
        var num14 = val;
        num14 = Math.floor((num14 / 10) % 13);
        var dgt15 = num14 % 10;
        if (val[14] == dgt15) {
            return '';
        } else {
            return 'Проверьте правильность номера';
        }
    }
}
/* Проверка ОГРН */


/* Проверка КПП */
function is_valid_kpp(i) {
    var val = i.val();
    if (val.length != 9) return 'Неверный формат номера';
    if (!val.match(/\d{4}[\dA-Z][\dA-Z]\d{3}/)) return 'Неверный формат номера';
    return '';
}
/* Проверка КПП */


/* Проверка ОКПО */
function is_valid_okpo(i) {
    var val = i.val();
    if (val.match(/\D/))return 'Неверный формат номера';
    if (val.length != 8 && val.length != 10) return 'Неверный формат номера';
    var okpo = val.match(/(\d)/g);
    var len = okpo.length;
    var sum = 0;
    var last_num = 0;
    var ostatok = 0;
    $.each(okpo, function (index, num) {
        index = index + 1;
        if (index != len)
            sum = sum + index * num;
        else
            last_num = num;
    });
    ostatok = sum % 11;
    if (ostatok == 10) {
        sum = 0;
        $.each(okpo, function (index, num) {
            index = index + 3;
            if (index != len)
                sum = sum + index * num;
            else
                last_num = num;
        });
        ostatok = sum % 11;
    }

    if (ostatok == last_num || (ostatok == 10 && last_num == 0)) {
        return '';
    }

    return 'Проверьте правильность номера';
}
/* Проверка ОКПО */


/* Проверка ОКТМО */
function is_valid_oktmo(i) {
    var val = i.val();
    if (val.match(/\D/))return 'Неверный формат номера';
    if (val.length != 8 && val.length != 11) return 'Неверный формат номера';

    return '';
}
/* Проверка ОКТМО */


/* Проверка ОКОПФ */
function is_valid_okopf(i) {
    var val = i.val();
    if (val.match(/\D/))return 'Неверный формат номера';
    if (val.length != 5) return 'Неверный формат номера';

    return '';
}
/* Проверка ОКОПФ */


/* Проверка ОКАТО */
function is_valid_okato(i) {
    var val = i.val();
    if (val.match(/\D/))return 'Неверный формат номера';
    if (val.length != 8 && val.length != 11) return 'Неверный формат номера';

    return '';
}
/* Проверка ОКАТО */


/* Проверка ОКФС */
function is_valid_okfs(i) {
    var val = i.val();
    if (val.match(/\D/))return 'Неверный формат номера';
    if (val.length != 2) return 'Неверный формат номера';

    return '';
}
/* Проверка ОКФС */


/* Проверка ОКОГУ */
function is_valid_okogu(i) {
    var val = i.val();
    if (val.match(/\D/))return 'Неверный формат номера';
    if (val.length != 5 && val.length != 7) return 'Неверный формат номера';

    return '';
}
/* Проверка ОКОГУ */


/* Проверка ОКОГУ */
function is_valid_okved(i) {
    var val = i.val();
    if (val.match(/\D/))return 'Неверный формат номера';
    if (val.length < 2 || val.length > 6) return 'Неверный формат номера';

    return '';
}
/* Проверка ОКОГУ */


/* Проверка БИК */
function is_valid_bank_bik(i) {
    var val = i.val();
    if (val.match(/\D/))return 'Неверный формат номера';
    if (val.length != 9) return 'Неверный формат номера';


    /* Автоматическая подстановка названия банка*/
    var rs = i.parents('.unit_wrapper').find('[rel="p_47_4"]');
    var cs = i.parents('.unit_wrapper').find('[rel="p_47_3"]');
    var name = i.parents('.unit_wrapper').find('[rel="p_47_2"]');
    $.post('/questionnaire/bik.php', {
            bik:    val
        }, function (content) {
            content = jQuery.parseJSON(content);

            cs.val(content.ks);
            SaveProperty(cs);
            name.val(content.name.replace(/&quot;/g, '"') + ' (' + content.namemini.replace(/&quot;/g, '"') + '), ' + content.city);
            SaveProperty(name);
            SaveProperty(rs);

            //alert(content.ks);
        }
    );
    return '';
}
/* Проверка БИК */


/* Проверка Расчетный счет */
function is_valid_bank_rs(i) {
    var bik = i.parents('.unit_wrapper').find('[rel="p_47_1"]');
    if(bik.val() == '')
        return 'Вначале введите БИК';
    /*if(is_valid_bank_bik(bik) != ''){
     return 'Неверный формат БИК';
     }*/

    // Необходимы 3 последние цифры БИКа var bik
    bik = bik.val().substr(6, 3);

    var val = i.val();
    if (val.match(/\D/))return 'Неверный формат номера';
    if (val.length != 20) return 'Неверный формат номера';
    val = bik + val;
    var account = val.match(/(\d)/g);
    var sum = 0;
    var veight = [7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1];

    $.each(account, function (index, num) {
        sum = sum + (num * veight[index]);
    });
    if ((sum % 10) == 0) {
        return '';
    }

    return 'Проверьте правильность номера';
}
/* Проверка Расчетный счет */


/* Проверка корреспондентского счёта */
function is_valid_bank_cs(i) {
    var bik = i.parents('.unit_wrapper').find('[rel="p_47_1"]');
    if(bik.val() == '')
        return 'Вначале введите БИК';
    /*if(is_valid_bank_bik(bik) != ''){
     return 'Неверный формат БИК';
     }*/
    // Необходимы два знака БИКа банка, начиная с пятого знака. var bik
    bik = bik.val().substr(4, 2);

    var val = i.val();
    if (val.match(/\D/))return 'Неверный формат номера';
    if (val.length != 20) return 'Неверный формат номера';
    val = '0' + bik + val;
    var account = val.match(/(\d)/g);
    var sum = 0;
    var veight = [7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1];

    $.each(account, function (index, num) {
        sum = sum + (num * veight[index]);
    });
    if ((sum % 10) == 0) {
        return '';
    }

    return 'Проверьте правильность номера';
}
/* Проверка корреспондентского счёта */

/*---------------------------------------------------------------------------------------------------*/


function ClearInput(input) {
    if (input.attr('type') == 'checkbox') {
        input.prop("checked", false);
    }
    else {
        input.val('');
        if(input.parents('.unit').hasClass('error')){
            input.parents('.unit').removeClass('error');
            input.next().removeClass('active');
            SaveProperty(input);
        }
    }
}

function ButtonIsDel(btn) {
    btn.attr('class', 'remove');
    btn.find('span').text('Убрать');
}
function ButtonIsAdd(btn) {
    btn.attr('class', 'add');
    btn.find('span').text('Указать');
}

/*Подсчет количества заполненных полей в блоке*/
/**
 * @return {number}
 */
function GetPercentage(section) {
    var countInput = 0;
    var compliteInput = 0;
    var progress = section.find('.progress');
    section.find('select:visible[myrequired], input:visible[myrequired]').each(function () {
        /*$(this).find('.required');*/
        countInput++;
        if ($(this).val() != '' && !$(this).next().hasClass('prompt')) {
            compliteInput++;
        }
    });
    var per = Math.round((compliteInput * 100) / countInput);
    if(per == 100 && progress.hasClass('incomplete')){
        progress.removeClass('incomplete').addClass('complete');
    } else if(per != 100 && progress.hasClass('complete')) {
        progress.removeClass('complete').addClass('incomplete');
    }
    progress.find('span').text(per);

    return per;
}
/*Подсчет количества заполненных полей в блоке*/


/*Проверка на заполненность обязательных полей*/
function CompleteForm() {
    var flag = true;
    $('.laststep, .step:visible').each(function () {
        if (GetPercentage($(this)) != 100) {
            flag = false;
            return false;
        }
    });
    if (flag) {
        $('input:submit').removeAttr("disabled");
    } else {
        $('input:submit').attr("disabled","disabled");
    }
}
/*Проверка на заполненность обязательных полей*/


/*Получение значения поля и отправка запроса по AJAX для сохранения значения в элементе инфоблока*/
function SaveProperty(input) {

    var validate = true;
    var parent_unit = input.parents(".unit");
    //if(parent_unit.hasClass('error')){
    parent_unit.find('.prompt').remove();
    parent_unit.removeClass('error');
    //}

    var propertyValue = $.trim(input.val());
    if(propertyValue){
        validate = ValidateInput(input);
    }
    if(!validate){
        return false;
    }
    var curSection = input.parents(".step");

    var status = parent_unit.find(".status");
    var property = input.attr('name').match(/([^\[]+)\[(\d+)\]\[*\d*\]*\[*([^\]]*)\]*/);
    var propertyType = property[1];
    var propertyID = property[2];
    var propertyKey = '0';


    if (propertyType == 'COMPOSITE_PROPERTY') {
        var parents = '';
        if(input.parents('.llc').length == 1)
            parents = '.llc ';
        else if(input.parents('.ie').length == 1)
            parents = '.ie ';
        else if(input.attr('class') == 'llcORie')
            parents = '.'+input.val()+'';
        var CproperyStr = '';
        input.parents(".unit_wrapper").find(parents+'input, .llcORie:checked').each(function () {
            if($(this).attr('class') == 'llcORie'){
                if($(this).prop("checked"))
                    propertyValue = ($(this).val() == 'llc') ? 'Юр. лицо' : 'Физ. лицо';
            } else if ($(this).attr('type') == 'checkbox') {
                propertyValue = ($(this).prop("checked")) ? 'Да' : 'Нет';
            } else{
                propertyValue = $(this).val();
            }
            //if (propertyValue != '') {
            var Cproperty = $(this).attr('name').match(/[^\[]+\[\d+\]\[*(\d)*\]*\[*([^\]]*)\]*/);
            propertyKey = Cproperty[1];
            CproperyStr = CproperyStr + '\n' + Cproperty[2] + ': ' + propertyValue;
            //}
        });
        propertyValue = CproperyStr;
    } else if (input.attr('type') == 'checkbox' && input.prop("checked") == false) {
        propertyValue = '0';
    }

    $.post('/questionnaire/ajax.php', {
            propertyID:    propertyID,
            propertyValue: propertyValue,
            propertyKey:   propertyKey
        }, function (content) {

            if (content == 'error')
                status.html('<span class="value">Ошибка сохранения</span>');
            else
                status.html('<span class="value saved">Сохранено</span>');
            setTimeout(function () {
                status.html('');
            }, 2000);
            GetPercentage(curSection);
            CompleteForm();
        }
    );
}
/*Получение значения элемента и отправка запроса по AJAX для сохранения значения в элементе инфоблока*/


/* Добавление дополнительных полей множественного свойства*/
function AddProperty(btn) {
    var btn_del = '<a class="del" href="javascript:void(0);">Убрать</a>';
    var unit_wrapper = btn.prev().clone();
    var header_next_number = unit_wrapper.find('.header span').text() * 1 + 1;
    btn.prev().find('.header .del').remove();
    var new_header = unit_wrapper.find('.header').html().replace(/(\d+)/, header_next_number);
    if (unit_wrapper.find('.header .del').length == 0)
        new_header = new_header + btn_del;

    unit_wrapper.find('.header').html(new_header);
    unit_wrapper.find('.unit').each(function () {
        var input_next_number = $(this).find('input').attr('name').match(/[^\[]+\[\d+\]\[(\d+)/);
        input_next_number = input_next_number[1] * 1 + 1;
        var name = $(this).find('input').attr('name').replace(/([^\[]+\[\d+\]\[)(\d+)/, '$1' + input_next_number);
        var input = $(this).find('input');
        input.attr('name', name);
        if($(this).attr('type') != 'radio')
            ClearInput($(this));
    });
    unit_wrapper.css('display', 'none');
    btn.before(unit_wrapper);
    unit_wrapper.slideDown(500);
    GetPercentage(btn.parents(".step"));
}
/* Добавление дополнительных полей множественного свойства*/


/* Удаление дополнительных полей множественного свойства */
function DelProperty(btn) {
    var unit_wrapper = btn.parents('.unit_wrapper');
    var btn_del = '<a class="del" href="javascript:void(0);">Убрать</a>';
    if (unit_wrapper.prev().prev().hasClass('unit_wrapper'))
        unit_wrapper.prev().find('.header').append(btn_del);
    unit_wrapper.slideUp(500);

    setTimeout(function () {
        GetPercentage(btn.parents(".step"));
        unit_wrapper.remove();

    }, 500);
}
/* Удаление дополнительных полей множественного свойства */


/* Показать\Убрать section.step */
function slideToggleSection(knopka) {
    var section = knopka.parent().next();
    if (section.css('display') == 'none') {
        section.slideDown(500).addClass('open');
        //section.find(['data-control']).click();
        ButtonIsDel(knopka);
        section.find('.progress span').html('0');
    } else {
        section.slideUp(500);
        ButtonIsAdd(knopka);
        section.find('input').each(function () {
            if($(this).val() != '') {
                $(this).val('');
                SaveProperty($(this));
            }
        });
        var first = true;
        section.find('.unit_wrapper').each(function () {
            if (first) {
                $(this).find('input').each(function () {
                    ClearInput($(this));
                });
                first = false;
            } else {
                $(this).remove();
            }
        });

    }
    setTimeout(function () {
        //GetPercentage(section);
        CompleteForm();
    }, 600);


}
function ShowHideSection(btn) {
    var section = btn.parent().next();
    section.find('input').each(function () {
        if ($(this).val() != '' || $(this).prop("checked") == true) {
            section.css("display", "block");
            ButtonIsDel(btn);
            return false;
        } else {
            ButtonIsAdd(btn);
            section.css("display", "none");
        }
    });
}
/* Показать\Убрать section.step */


/* Показывать дополнительный input, если выбран нужный пункт в select */
function InputSlideToggle(obj) {
    var parent = $(this).parents('.step');
    if (obj.find('option[value="' + obj.val() + '"]').text() == 'Другая') {
        obj.parents('.unit').next().slideDown(600);
    }
    else if(obj.parents('.unit').next().css('display') == 'table' || obj.parents('.unit').next().css('display') == 'block') {
        obj.parents('.unit').next().css('display', 'block').slideUp(600);
    }
    setTimeout(function () {
        GetPercentage(obj.parents(".step"));
    }, 700);

}
/* Показывать дополнительный input, если выбран нужный пункт в select */


/* Показать\Скрыть блок с дополнительными input */
function ShowHideWrapper(checkbox) {
    var property_id = checkbox.attr('id');
    var unit_wrapper = $('[rel="' + property_id + '"]');
    if (checkbox.prop('checked')) {
        unit_wrapper.slideUp(500);
        setTimeout(function () {
            if (unit_wrapper.find('.checked').length != 0) {
                unit_wrapper.find(".checked").css("display", "block");
                unit_wrapper.find(".unchecked").css("display", "none").find('input').each(function () {
                    ClearInput($(this));
                });
            }
            unit_wrapper.slideDown(500);
            GetPercentage(checkbox.parents(".step"));
        }, 500);
    } else {
        unit_wrapper.slideUp(500);
        if (unit_wrapper.find('.checked').length != 0) {
            unit_wrapper.slideDown(500);
            setTimeout(function () {
                unit_wrapper.find(".unchecked").css("display", "block");
                unit_wrapper.find(".checked").css("display", "none").find('input').each(function () {
                    ClearInput($(this));
                });
                GetPercentage(checkbox.parents(".step"));
            }, 500);
        } else {
            unit_wrapper.find('input').each(function () {
                ClearInput($(this));
            });
        }
    }

}
/* Показать\Скрыть блок с дополнительными input */


/* Показать\Скрыть разные блоки для ЮР и ФИЗ при выборе радиокнопки */
function ShowHide2Wrapper(radio) {
    console.log(radio);
    var type = radio.val();
    var parent = radio.parents('.unit_wrapper');
    var llc = parent.find('.llc');
    var ie = parent.find('.ie');
    if(type == 'llc'){
        ie.slideUp(500);
        setTimeout(function () {
            llc.slideDown(600);
            ie.find('input').each(function () {
                   ClearInput($(this));

            });
            GetPercentage(radio.parents(".step"));
        }, 500);
    } else if(type == 'ie'){
        llc.slideUp(500);
        setTimeout(function () {
            ie.slideDown(600);
            llc.find('input').each(function () {
                ClearInput($(this));
            });
            GetPercentage(radio.parents(".step"));
        }, 500);

    }
}
/* Показать\Скрыть разные блоки для ЮР и ФИЗ при выборе радиокнопки */


function strToAddressForm(el){
    var val = '';
    if (el) val = el.val();

    var arr_val = val.split(',');
    var ar_el_value = [];
    $.each(arr_val, function( index, value ) {
        var type = value.split(' - ');
        ar_el_value[$.trim(type[0])] = $.trim(type[1]);
    });
    var name_input = '';
    $('#address').find('input').each(function () {
        $(this).val('');
        name_input = $(this).parent().prev().find('.name').text();
        if (ar_el_value[name_input]) {
            $(this).val(ar_el_value[name_input]);
        }
    });
    if($('#id_w').val() == '')
        $('#id_w').val('Россия');
}

function changeInput_col_chk(el){
    if (el.prop('checked'))
        strToAddressForm($('[rel="p_19"]'));
    else
        strToAddressForm(false);
}
function changeInput_col_radio(el){
    if (el.attr('id') == 'id_1')
        strToAddressForm($('[rel="p_19"]'));
    else if (el.attr('id') == 'id_2')
        strToAddressForm($('[rel="p_20"]'));
}


$(document).ready(function () {
    $('.col_chk').on('change', 'input', function(){
        changeInput_col_chk($(this));
    });
    $('.col_radio').on('change', '[name="sov"]', function(){
        changeInput_col_radio($(this));
    });

    $('.date').mask("99.99.9999");
    $('#id_q').mask("999999");


    $('.step, .laststep').on('change', 'input, select', function () {
        SaveProperty($(this));
    }).on('click', '.add', function () {
        AddProperty($(this))
    }).on('click', '.del', function () {
        DelProperty($(this));
    }).on('focus', 'input', function () {
        var el = $(this);
        var val = $(this).val();
        var rel = $(this).attr('rel');
        var el_class = $(this).attr('class');

        if(el_class == 'address' || rel == 'p_19' || rel == 'p_20' || rel == 'p_21' || rel == 'p_111' || rel == 'p_112') {
//"Это для адреса"
///////////////////////////////////////////////////////////////////
            strToAddressForm(el);
            if (rel == 'p_20') {
                $('.col_chk').css('display', 'block');
                $('.col_radio').css('display', 'none');
            }
            else if (rel == 'p_21') {
                $('.col_radio').css('display', 'block');
                $('.col_chk').css('display', 'none');
                /*$('.col_radio [name="sov"]').on('change', function () {
                    if ($(this).attr('id') == 'id_1')
                        strToAddressForm(el.parents('.unit').prev().prev().find('input'));
                    else if ($(this).attr('id') == 'id_2')
                        strToAddressForm(el.parents('.unit').prev().find('input'));
                    else
                        strToAddressForm(false);
                })*/
            }
            else {
                $('.col_chk').css('display', 'none');
                $('.col_radio').css('display', 'none');
                strToAddressForm(el);
            }
            $('#address').bPopup({
                speed:      250,
                transition: 'slideUp'
            });
            $('#address .btn').click(function(){
                var str = '';
                $('#address').find('input').each(function () {
                    if($(this).val() != ''){
                        str = str + $(this).parent().prev().find('.name').text() + ' - ' +$(this).val() + ', ';
                    }

                });
                el.val(str.slice(0,-2));
                SaveProperty(el);
                $($(this)).off('click');
            });

        }
///////////////////////////////////////////////////////////////////
        if(rel == 'p_22' || rel == 'p_57' || rel == 'p_113') {
//"Это для телефона"
///////////////////////////////////////////////////////////////////
            var arr_val = val.split(' ');
            $('#country').val((arr_val[0] == '')?'+7':arr_val[0]);
            $('#city').val(arr_val[1]);
            $('#number').val(arr_val[2]);
            if(arr_val[1] == '' || arr_val[2] == '' || arr_val[0] == '')
                $('#phone .btn').addClass('disabled');

            $('#phone').bPopup({
                speed: 250,
                transition: 'slideUp'
            });
            $('#country').bind("keyup", function() {
                if (this.value.match(/[^0-9\+]/g)) {
                    this.value = this.value.replace(/[^0-9\+]/g, '');
                }
                var len = this.value.length;
                if(len > 2)
                    $(this).val($(this).val().substr(0, 2));
            });

            $('#city').bind("keyup", function() {
                if (this.value.match(/[^0-9]/g)) {
                    this.value = this.value.replace(/[^0-9]/g, '');
                }
                var val = this.value;
                var len = val.length;
                if(len > 5)
                    $(this).val($(this).val().substr(0, 5));

                $(this).parents('.item').removeClass('error');
                if(len == 3){
                    $('#number').mask("999-99-99",{completed:function(){$('#phone .btn').removeClass('disabled');}});
                } else if(len == 4){
                    $('#number').mask("99-99-99",{completed:function(){$('#phone .btn').removeClass('disabled');}});
                } else if(len == 5 || len == 6){
                    $('#number').mask("9-99-99",{completed:function(){$('#phone .btn').removeClass('disabled');}});
                } else {
                    $(this).parents('.item').addClass('error');
                    $('#number').mask('Введите код');
                    $('#phone .btn').addClass('disabled');
                }
            });

            $('#phone').on('click', '.btn', function(){
                var str = '';
                var country = '';
                if($('#phone #country').val() == '')
                    country = '+7';
                else
                    country = $('#phone #country').val();
                str = country + ' ' + $('#phone #city').val() + ' ' + $('#phone #number').val();

                el.val(str);
                SaveProperty(el);
            });
        }
///////////////////////////////////////////////////////////////////






        var val_new = '';
        if($(this).parents('.unit').hasClass('error')){
            $(this).parents('.unit').removeClass('error');
            $(this).next().removeClass('active');

            $(this).blur(function(){
                val_new = $(this).val();
                if(val == val_new){
                    $(this).parents('.unit').addClass('error');
                    $(this).next().addClass('active');
                }
            });
        }

    });

    $('.step').on('change', '.llcORie', function () {
        ShowHide2Wrapper($(this));
    });

    $('.llcORie:checked').each(function () {
        ShowHide2Wrapper($(this));
    });



    $('.btnWrp a').each(function () {
        ShowHideSection($(this));
    }).on('click', function () {
        slideToggleSection($(this));
    });

    $('select').each(function () {
        InputSlideToggle($(this));
    }).change(function () {
        InputSlideToggle($(this));
    });

    $('.checkbox input').each(function () {
        ShowHideWrapper($(this));
    }).on('click', function () {
        ShowHideWrapper($(this));
    });

    $('.step:visible').each(function () {
        GetPercentage($(this));
    });
    setTimeout(function () {
        CompleteForm();
    }, 1000);

    $('form').submit(function (obj) {

        obj.preventDefault();
        $.post('/questionnaire/ajax.php', {submit: 'complete'}, function (content) {
                if (content != 'error')
                    window.location.reload();
            }
        );
    });

    setTimeout(function () {
        if($('[rel=p_168_5]').val() == '')
            $('[rel=p_168_5]').val('Устав');
    }, 5000);
});