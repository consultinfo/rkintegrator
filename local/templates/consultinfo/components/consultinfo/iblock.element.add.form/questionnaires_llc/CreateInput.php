<?
    function CreateCompositeInputs($propertyID, $arResult, $arParams, $nameBlock, $arNameInput, $ar2NameInput = array())
    {
        foreach ($arResult["ELEMENT_PROPERTIES"][ $propertyID ] as $keyComposite => $value) { ?>
            <div class="unit_wrapper">
                <div class="header clearFix"><?=$nameBlock?> <span><?=$keyComposite + 1?></span></div>

                <?if ($nameBlock == 'Учредитель' && !empty($ar2NameInput)):
                    $nameInput = 'Вид учредителя';
                    $typeInput = 'radio';
                    $flag = false;
                    $numInput = 0;

                    //$value = $arResult["ELEMENT_PROPERTIES"][ $propertyID ][ $counter ]["VALUE"];
                    //print_r($value);
                ?>
                <div class="radioWrap">
                    <?CreateInput($propertyID, $arResult, $arParams, $nameInput, $keyComposite + 1, $numInput, $typeInput, $flag
                    );?>
                </div>
                <div class="llc">
                    <?endif; ?>
                    <?$numInput=0;
                        if ($nameBlock == 'Учредитель' && !empty($ar2NameInput)) {
                            $numInput = $numInput + 1;
                        }
                        foreach ($arNameInput as $name => $flag) {
                            $nameInput = $name;
                            $typeInput = $flag[1];

                        CreateInput($propertyID, $arResult, $arParams, $nameInput, $keyComposite + 1, $numInput,
                                    $typeInput, $flag
                        );
                            $numInput++;
                    } ?>

                    <?if ($nameBlock == 'Учредитель' && !empty($ar2NameInput)): ?>
                </div>
                <div class="ie">
                    <?$numInput=0;
                        if ($nameBlock == 'Учредитель' && !empty($ar2NameInput)) {
                            $numInput = $numInput + 1;
                        }
                        foreach ($ar2NameInput as $name => $flag) {
                            $nameInput = $name;
                            $typeInput = $flag[1];

                            CreateInput($propertyID, $arResult, $arParams, $nameInput, $keyComposite + 1, $numInput,
                                        $typeInput, $flag
                            );
                            $numInput++;
                        } ?>
                </div>
            <?endif; ?>
            </div>
        <?
        }
    }


    function CreateInput(
        $propertyID,
        $arResult,
        $arParams,
        $nameInput = '',
        $keyComposite = '',
        $numInput = '',
        $typeInput = '',
        $flag = false
    )
    {
        global $APPLICATION; ?>
        <?if ($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["GetPublicEditHTML"]) {
        $INPUT_TYPE = "USER_TYPE";
    } else {
        $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"];
    }

        if ($typeInput == 'checkbox' || $typeInput == 'radio') {
            $INPUT_TYPE = 'L';
        } else if($typeInput == 'date') {
            $INPUT_TYPE = 'S';
        }

        ?>

        <div class="unit">
            <?if ($INPUT_TYPE != 'L') { ?>
                <div class="label">
                <span class="value">
                    <?=($nameInput == '') ? $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["NAME"] : $nameInput;?>
                    <?=((in_array($propertyID, $arResult["PROPERTY_REQUIRED"]) && $keyComposite=='')|| ($flag && $keyComposite!='')) ? '<span class="required">*</span>' :
                        '';?>
                </span>
                </div>
            <? } else { ?>

            <?
            } ?>



            <?if (intval($propertyID) > 0) {
                if (
                    $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] == "T"
                    &&
                    $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"] == "1"
                ) {
                    $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] = "S";
                } elseif (
                    (
                        $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] == "S"
                        ||
                        $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] == "N"
                    )
                    &&
                    $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"] > "1"
                ) {
                    $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] = "T";
                }
            } elseif (($propertyID == "TAGS") && CModule::IncludeModule('search')) {
                $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] = "TAGS";
            }

                $counter = 0;
                if ($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["MULTIPLE"] == "Y") {
                    $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ?
                        count($arResult["ELEMENT_PROPERTIES"][ $propertyID ]) : 0;
                    //$inputNum += $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["MULTIPLE_CNT"];
                } else {
                    $inputNum = 1;
                }
                if ($nameInput != '') {
                    $inputNum = $keyComposite;
                    $counter = $keyComposite - 1;
                }

                if ($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["USER_TYPE"] == 'Date') {
                    $INPUT_TYPE = 'S';
                }

                switch ($INPUT_TYPE):

                    case "S":
                    case "N":
                        for ($i = $counter; $i < $inputNum; $i++) {
                            if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                $value = intval($propertyID) > 0 ?
                                    $arResult["ELEMENT_PROPERTIES"][ $propertyID ][ $i ]["VALUE"] :
                                    $arResult["ELEMENT"][ $propertyID ];
                            } elseif ($i == 0) {
                                $value = intval($propertyID) <= 0 ? "" :
                                    $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["DEFAULT_VALUE"];
                            } else {
                                $value = "";
                            }

                            /**
                             * 25 - Сведения об учредителях и бенифициарных владельца
                             */ ?>
                            <div class="value">
                                <?if ($nameInput != '') {
                                    $arValue = explode("\n", $value);
                                    if ($arValue[0] == '') {
                                        $numInput = $numInput + 1;
                                    }

                                    $arValue = explode(': ', $arValue[ $numInput ]);
                                    ?>
                                    <input <?=(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])) ? 'myrequired' :
                                        '';?> type="text"
                                              <?=($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["USER_TYPE"] == "Date" || $typeInput=='date')?'class="date"':''?>
                                              <?=($typeInput=='address')?'class="address"':''?>
                                              name="COMPOSITE_PROPERTY[<?=$propertyID?>][<?=$i?>][<?=$nameInput?>]"
                                              value="<?=$arValue[1]?>"
                                              rel="p_<?=$propertyID?>_<?=$numInput?>"/>
                                <? } else { ?>
                                    <input <?=(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])) ? 'myrequired' :
                                        '';?> rel="p_<?=$propertyID?>"
                                        <?=($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["USER_TYPE"] == "Date" || $typeInput=='date')?'class="date"':''?>
                                        <?=($typeInput=='address')?'class="address"':''?>
                                        type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]"
                                              value="<?=$value?>" />
                                <?
                                } ?>
                                <?/*if ($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["USER_TYPE"] == "Date"): ?>
                                    <span class="calendar">
                            <? $APPLICATION->IncludeComponent(
                                'bitrix:main.calendar',
                                '',
                                array(
                                    'FORM_NAME'   => 'iblock_add',
                                    'INPUT_NAME'  => "PROPERTY[" . $propertyID . "][" . $i . "]",
                                    'INPUT_VALUE' => $value,
                                ),
                                null,
                                array('HIDE_ICONS' => 'Y')
                            ); ?>
                        </span>
                                <? endif; */?>
                            </div>
                        <?
                        }
                        break;
                    case "L":

                        if ($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["LIST_TYPE"] == "C") {
                            $type = $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["MULTIPLE"] == "Y" ? "checkbox" :
                                "radio";
                        } else {
                            $type = $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["MULTIPLE"] == "Y" ? "multiselect" :
                                "dropdown";
                        }

                        if ($typeInput == 'checkbox') {
                            $type = 'checkbox';
                        }
                        if ($typeInput == 'radio') {
                            $type = 'radio';
                        }

                        if ($type == 'checkbox' || $type == 'dropdown') { ?>
                            <div class="label">
                    <span class="value">
                        <?=($typeInput == 'checkbox') ? $nameInput :
                            $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["NAME"];?>
                        <?=(in_array($propertyID, $arResult["PROPERTY_REQUIRED"]) && $typeInput != 'checkbox') ? '<span class="required">*</span>' :
                            '';?>
                    </span>
                            </div>
                        <?
                        }
                        switch ($type):
                            case "checkbox":
                                if ($typeInput == 'checkbox') {

                                    $value = $arResult["ELEMENT_PROPERTIES"][ $propertyID ][ $counter ]["VALUE"];

                                    $arValue = explode("\n", $value);
                                    if ($arValue[0] == '') {
                                        $numInput = $numInput + 1;
                                    }
                                    $arValue = explode(': ', $arValue[ $numInput ]);

                                    ?>
                                    <div class="value">
                                        <label class="<?=$type?>">
                                            <input type="<?=$type?>"
                                                   name="COMPOSITE_PROPERTY[<?=$propertyID?>][<?=$counter?>][<?=$nameInput?>]" <? if ($arValue[1] ==
                                                'Да'
                                            ) {
                                                echo 'checked';
                                            } ?> value="Да" />
                                            <span>&nbsp;</span>
                                        </label>
                                    </div>
                                <? } else {
                                    foreach ($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ENUM"] as $key => $arEnum)
                                    {
                                        $checked = false;
                                        if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                            if (is_array($arResult["ELEMENT_PROPERTIES"][ $propertyID ])) {
                                                foreach ($arResult["ELEMENT_PROPERTIES"][ $propertyID ] as $arElEnum) {
                                                    if ($arElEnum["VALUE"] == $key) {
                                                        $checked = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        } else {
                                            if ($arEnum["DEF"] == "Y") $checked = true;
                                        }

                                        ?>
                                        <div class="value">
                                            <label class="<?=$type?>" for="property_<?=$key?>">
                                                <input type="<?=$type?>"
                                                       name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ?
                                                           "[" . $key . "]" : ""?>"
                                                       value="<?=$key?>"
                                                       id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" :
                                                    ""?> />
                                                <span>&nbsp;</span>
                                            </label>
                                        </div>
                                    <?
                                    }
                                };
                                break;
                            case "radio":
                                if ($typeInput == 'radio') {

                                    $value = $arResult["ELEMENT_PROPERTIES"][ $propertyID ][ $counter ]["VALUE"];

                                    $arValue = explode("\n", $value);
                                    if ($arValue[0] == '') {
                                        $numInput = $numInput + 1;
                                    }
                                    $arValue = explode(': ', $arValue[ $numInput ]);
                                    ?>

                                    <div class="item">
                                        <label class="radio">
                                            <span class="label">Юр. лицо</span>
                                            <input type="<?=$type?>"
                                                   name="COMPOSITE_PROPERTY[<?=$propertyID?>][<?=$counter?>][<?=$nameInput?>]"
                                                   value="llc"
                                                <?if($arValue[1] == 'Юр. лицо'){echo 'checked';}?>
                                                   class="llcORie"
                                                />
                                            <span class="rbutton">&nbsp;</span>
                                        </label>
                                    </div>
                                    <div class="item">
                                        <label class="radio">
                                            <span class="label">Физ. лицо</span>
                                            <input type="<?=$type?>"
                                                   name="COMPOSITE_PROPERTY[<?=$propertyID?>][<?=$counter?>][<?=$nameInput?>]"
                                                   value="ie"
                                                <?if($arValue[1] == 'Физ. лицо' || ($arValue[1] == '')){echo 'checked';}?>
                                                   class="llcORie"
                                                />
                                            <span class="rbutton">&nbsp;</span>
                                        </label>
                                    </div>





                                <? } else {





                                    $myflag = false;
                                    foreach ($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ENUM"] as $key => $arEnum) {
                                        $checked = false;
                                        if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                            if (is_array($arResult["ELEMENT_PROPERTIES"][ $propertyID ])) {
                                                foreach ($arResult["ELEMENT_PROPERTIES"][ $propertyID ] as $arElEnum) {
                                                    if ($arElEnum["VALUE"] == $key) {
                                                        $checked = true;
                                                        $myflag = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        } else {
                                            if ($arEnum["DEF"] == "Y"){
                                                $checked = true;
                                                $myflag = true;
                                            }

                                        }
                                        if($arEnum == end($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ENUM"]) && !$myflag) {
                                            $checked = true;
                                        }

                                        ?>
                                        <div class="item">
                                            <label class="radio" for="property_<?=$key?>">
                                                <span class="label"><?=$arEnum["VALUE"]?></span>
                                                <input type="<?=$type?>"
                                                       name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ?
                                                           "[" . $key . "]" : ""?>"
                                                       value="<?=$key?>"
                                                       id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> />
                                                <span class="rbutton">&nbsp;</span>
                                            </label>
                                        </div>
                                    <?
                                    }
                                }
                                break;

                            case "dropdown":
                            case "multiselect":
                                ?>
                                <div class="value">
                                    <select
                                        name="PROPERTY[<?=$propertyID?>]<?=$type == "multiselect" ?
                                            "[]\" size=\"" .
                                            $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"] .
                                            "\" multiple=\"multiple" : ""?>">
                                        <option
                                            value=""><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
                                        <?
                                            if (intval($propertyID) > 0) {
                                                $sKey = "ELEMENT_PROPERTIES";
                                            } else $sKey = "ELEMENT";

                                            foreach (
                                                $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ENUM"] as $key =>
                                                $arEnum
                                            ) {
                                                $checked = false;
                                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                                    foreach ($arResult[ $sKey ][ $propertyID ] as $elKey => $arElEnum) {
                                                        if ($key == $arElEnum["VALUE"]) {
                                                            $checked = true;
                                                            break;
                                                        }
                                                    }
                                                } else {
                                                    if ($arEnum["DEF"] == "Y") $checked = true;
                                                }
                                                ?>
                                                <option
                                                    value="<?=$key?>" <?=$checked ? " selected=\"selected\"" :
                                                    ""?>><?=$arEnum["VALUE"]?></option>
                                            <?
                                            }
                                        ?>
                                    </select>
                                </div>
                                <?
                                break;

                        endswitch;
                        break;
                endswitch; ?>
            <div class="status">

            </div>
        </div>
    <?
    }?>