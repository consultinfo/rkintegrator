<?
    if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
    $this->setFrameMode(false);
    require('CreateInput.php');
?>
<div class="container light">
    <div class="static clearFix">
        <div class="app clearFix">
            <h4>
                Во время заполнения анкета сохраняется в черновике. Вы можете возобновить работу по ней в любой удобный
                момент, перейдя по ссылке из письма. </h4>
        </div>
    </div>
</div>
<div class="container">
    <div class="static">
        <div id="app">
            <div class="ctrls clearFix">
                <ul class="tabs triggers">
                    <li class="active">
                        <a class="block" data-target="#coltd" href="javascript:void(0);">Анкета для компаний (ООО)</a>
                    </li>
                </ul>
            </div>
        </div>
        <section class="app">
            <h5 class="none">application forms</h5>

            <div class="tabs-content">
                <? /*
                    if (!empty($arResult["ERRORS"]))
                        ShowError(implode("<br />", $arResult["ERRORS"]));
                    if (strlen($arResult["MESSAGE"]) > 0)
                        ShowNote($arResult["MESSAGE"]);
                */ ?>
                <form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
                    <div id="coltd" class="tabs-pane active">
                        <section class="app-form" data-accordion-group>
                            <h5 class="none">applicationn step</h5>
                            <?=bitrix_sessid_post()?>
                            <? if ($arParams["MAX_FILE_SIZE"] > 0) { ?>
                                <input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" />
                            <? } ?>
                            <!--<input type="hidden" value="Название" name="PROPERTY[NAME][0]">-->


                            <!--Базовая информация-->
                            <?if($arResult['ELEMENT_PROPERTIES']['134'][0]['VALUE_ENUM'] == 'да'){?>
                            <section class="step" data-accordion>
                                <h5 class="none">application step</h5>
                                <a data-control href="javascript:void(0);">Базовая информация</a>
                                <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                <div class="content" data-content>
                                    <article class="wrapper noselect">
                                        <h5 class="none">inner of the app step</h5>
                                        <? CreateInput(1, $arResult, $arParams) ?>
                                        <? CreateInput(2, $arResult, $arParams) ?>
                                        <? CreateInput(3, $arResult, $arParams) ?>
                                        <? CreateInput(4, $arResult, $arParams) ?>
                                        <? CreateInput(5, $arResult, $arParams) ?>
                                        <? CreateInput(133, $arResult, $arParams) ?>
                                        <? CreateInput(6, $arResult, $arParams) ?>
                                        <div class="unit_wrapper" rel="property_24">
                                            <div class="checked">
                                                <? CreateInput(7, $arResult, $arParams) ?>
                                                <? CreateInput(8, $arResult, $arParams) ?>
                                                <? CreateInput(9, $arResult, $arParams) ?>
                                                <? CreateInput(10, $arResult, $arParams) ?>
                                                <div class="header">Коды</div>
                                                <div class="parts clearFix">
                                                    <div class="part">
                                                        <? CreateInput(11, $arResult, $arParams) ?>
                                                        <? CreateInput(12, $arResult, $arParams) ?>
                                                        <? CreateInput(13, $arResult, $arParams) ?>
                                                        <? CreateInput(14, $arResult, $arParams) ?>
                                                    </div>
                                                    <div class="part">
                                                        <? CreateInput(15, $arResult, $arParams) ?>
                                                        <? CreateInput(16, $arResult, $arParams) ?>
                                                        <? CreateInput(17, $arResult, $arParams) ?>
                                                        <? CreateInput(18, $arResult, $arParams) ?>
                                                    </div>
                                                </div>
                                                <div class="header">Статусы</div>
                                                <? CreateInput(45, $arResult, $arParams) ?>
                                                <? CreateInput(46, $arResult, $arParams) ?>
                                            </div>
                                            <div class="unchecked">
                                                <? CreateInput(75, $arResult, $arParams) ?>
                                                <? CreateInput(76, $arResult, $arParams) ?>
                                                <? CreateInput(77, $arResult, $arParams) ?>
                                                <? CreateInput(78, $arResult, $arParams) ?>
                                                <? CreateInput(79, $arResult, $arParams) ?>
                                                <? CreateInput(80, $arResult, $arParams) ?>
                                            </div>
                                        </div>


                                        <div class="header">Единоличные исполнительные органы</div>
                                        <div class="context">генеральный директор, директор, президент и т.д.</div>
                                        <? CreateCompositeInputs(168, $arResult, $arParams, 'Исполнительный орган',
                                                                 array(
                                                                     'Фамилия' => array(true, ''),
                                                                     'Имя' => array(true, ''),
                                                                     'Отчество' => array(true, ''),
                                                                     'Должность' => array(true, ''),
                                                                     'Основание полномочий' => array(true, ''),
                                                                     'Дата начала полномочий' => array(true, 'date'),
                                                                     'Дата окончания полномочий' => array(true, 'date')
                                                                 )
                                        ) ?>
                                        <a class="add" href="javascript:void(0);">Добавить исполнительный орган</a>
                                    </article>
                                </div>
                            </section>
                            <?}?>
                            <!--/Базовая информация-->
                            <!--Адреса юридического лица-->
                            <?if($arResult['ELEMENT_PROPERTIES']['135'][0]['VALUE_ENUM'] == 'да'){?>
                            <section class="step" data-accordion>
                                <h5 class="none">application step</h5>
                                <a data-control href="javascript:void(0);">Адреса юридического лица</a>
                                <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                <div class="content" data-content>
                                    <article class="wrapper noselect">
                                        <h5 class="none">inner of the app step</h5>
                                        <? CreateInput(19, $arResult, $arParams) ?>
                                        <? CreateInput(20, $arResult, $arParams) ?>
                                        <? CreateInput(21, $arResult, $arParams) ?>
                                        <? CreateInput(22, $arResult, $arParams) ?>
                                        <?// CreateInput(23, $arResult, $arParams) ?>
                                        <? CreateInput(24, $arResult, $arParams) ?>
                                    </article>
                                </div>
                            </section>
                            <?}?>
                            <!--/Адреса юридического лица-->
                            <!--Учредители и бенефициарные владельцы-->
                            <?if($arResult['ELEMENT_PROPERTIES']['136'][0]['VALUE_ENUM'] == 'да'){?>
                            <section class="step" data-accordion>
                                <h5 class="none">application step</h5>
                                <a data-control href="javascript:void(0);">Собственники и бенефициарные владельцы</a>
                                <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                <div class="content" data-content>
                                    <article class="wrapper noselect">
                                        <h5 class="none">inner of the app step</h5>

                                        <div class="context">А также сведения о лицах, которые имеют право давать
                                            обязательные для юр.
                                            лица указания, либо иным образом имеют возможность
                                            определять его действия,
                                            в том числе
                                            сведения об основном обществе или преобладающем,
                                            участвующем обществе (доля
                                            дочерних или
                                            зависимых обществ), холдинговой или финансово-промышленной
                                            группе (если юр.
                                            лицо в ней
                                            участвует), бенефициарные владельцы
                                        </div>


                                        <? CreateCompositeInputs(25, $arResult, $arParams, 'Учредитель',
                                                                 array(
                                                                     'Наименование'                                                                                             => array(true, ''),
                                                                     'ОГРН'                                                                                                     => array(true, ''),
                                                                     'ИНН'                                                                                                      => array(true, ''),
                                                                     'Адрес места нахождения'                                                                                   => array(true, 'address'),
                                                                     'Доля в капитале юр. лица, %'                                                                              => array(true, ''),
                                                                     'Участник федеральных целевых программ или национальных проектов, либо резидент особой экономической зоны' => array(false, 'checkbox'),
                                                                     'Наличие в уставном капитале государственного участия'                                                     => array(false, 'checkbox')
                                                                 ),
                                                                 array(
                                                                     'ФИО'                                                                                                      => array(true, ''),
                                                                     'Дата рождения'                                                                                            => array(true, 'date'),
                                                                     'Серия документа'                                                                                          => array(false, ''),
                                                                     'Номер документа'                                                                                          => array(true, ''),
                                                                     'Адрес места жительства'                                                                                   => array(true, 'address'),
                                                                     'Доля в капитале юр. лица, %'                                                                              => array(true, '')
                                                                 )
                                        ) ?>


                                        <a class="add" href="javascript:void(0);">Добавить учредителя</a>
                                    </article>
                                </div>
                            </section>
                            <?}?>
                            <!--/Учредители и бенефициарные владельцы-->
                            <!--Органы юридического лица-->
                            <?if($arResult['ELEMENT_PROPERTIES']['137'][0]['VALUE_ENUM'] == 'да'){?>
                            <section class="step" data-accordion>
                                <h5 class="none">application step</h5>
                                <a data-control href="javascript:void(0);">Сведения об органах юридического лица (кроме единоличного исполнительного органа)</a>
                                <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                <div class="content" data-content>
                                    <article class="wrapper noselect">
                                        <h5 class="none">inner of the app step</h5>

                                        <div class="context">Структура органов управления, персональный состав органов
                                            управления юр. лица
                                        </div>
                                        <? CreateCompositeInputs(34, $arResult, $arParams, 'Орган управления',
                                                                 array(
                                                                     'Наименование органа управления' => array(true, ''),
                                                                     'Персональный состав'            => array(true, ''),
                                                                     'Полномочия'                     => array(true, '')
                                                                 )
                                        ) ?>
                                        <a class="add" href="javascript:void(0);">Добавить орган управления</a>

                                        <div class="text">Сведения о присутствии или отсутстви по адресу места
                                            нахождения юридического лица его постоянно действующих органов
                                            или лиц,
                                            имеющих право действовать от имени юридического лица без
                                            доверенности
                                        </div>
                                        <div class="radioWrap">
                                            <? CreateInput(37, $arResult, $arParams) ?>
                                        </div>
                                    </article>
                                </div>
                            </section>
                            <?}?>
                            <!--/Органы юридического лица-->
                            <!--Аффилированные лица-->
                            <?if($arResult['ELEMENT_PROPERTIES']['138'][0]['VALUE_ENUM'] == 'да'){?>
                            <section class="step" data-accordion>
                                <h5 class="none">application step</h5>
                                <a href="javascript:void(0);" data-control>Аффилированные лица</a>
                                <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                <div class="content" data-content>
                                    <article class="wrapper noselect">
                                        <h5 class="none">inner of the app step</h5>

                                        <? CreateCompositeInputs(38, $arResult, $arParams, 'Аффилированное лицо',
                                                                 array(
                                                                     'Фамилия'  => array(true, ''),
                                                                     'Имя'      => array(true, ''),
                                                                     'Отчество' => array(true, '')
                                                                 )
                                        ) ?>
                                        <a class="add" href="javascript:void(0);">Добавить аффилированое лицо</a>
                                    </article>
                                </div>
                            </section>
                            <?}?>
                            <!--/Аффилированные лица-->
                            <!--Величина уставного капитала-->
                            <?if($arResult['ELEMENT_PROPERTIES']['139'][0]['VALUE_ENUM'] == 'да'){?>
                            <section class="step" data-accordion>
                                <h5 class="none">application step</h5>
                                <a href="javascript:void(0);" data-control>Величина уставного капитала и
                                    стоимость имущества</a>
                                <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                <div class="content" data-content>
                                    <article class="wrapper noselect">
                                        <h5 class="none">inner of the app step</h5>
                                        <? CreateInput(39, $arResult, $arParams) ?>
                                        <? CreateInput(74, $arResult, $arParams) ?>
                                        <? CreateInput(41, $arResult, $arParams) ?>
                                    </article>
                                </div>
                            </section>
                            <?}?>
                            <!--/Величина уставного капитала-->
                            <!--Реквизиты банковского счета в рублях-->
                            <?if($arResult['ELEMENT_PROPERTIES']['141'][0]['VALUE_ENUM'] == 'да'){?>
                                <section class="step" data-accordion>
                                    <h5 class="none">application step</h5>
                                    <a href="javascript:void(0);" data-control>Реквизиты банковского счета в рублях</a>
                                    <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                    <div class="content" data-content>
                                        <article class="wrapper noselect">
                                            <h5 class="none">inner of the app step</h5>
                                            <? CreateCompositeInputs(47, $arResult, $arParams, 'Банковский счет',
                                                                     array(
                                                                         'БИК'                                   => array(true, ''),
                                                                         'Наименование банка с указанием города' => array(true, ''),
                                                                         'К/с'                                   => array(true, ''),
                                                                         'Номер расчетного счета'                => array(true, ''),
                                                                     )
                                            ) ?>
                                            <a class="add" href="javascript:void(0);">Добавить еще один банковский счет</a>
                                        </article>
                                    </div>
                                </section>
                            <?}?>
                            <!--/Реквизиты банковского счета в рублях-->
                            <!--Обособленные подразделения-->
                            <?if($arResult['ELEMENT_PROPERTIES']['140'][0]['VALUE_ENUM'] == 'да'){?>
                            <div class="btnWrp">
                                <a class="add" href="javascript:void(0);"><span>Указать</span> обособленные
                                    подразделения</a>
                            </div>
                            <section class="step" data-accordion>
                                <h5 class="none">application step</h5>
                                <a href="javascript:void(0);" data-control>Обособленные подразделения</a>
                                <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                <div class="content" data-content>
                                    <article class="wrapper noselect">
                                        <h5 class="none">inner of the app step</h5>

                                        <? CreateCompositeInputs(43, $arResult, $arParams, 'Обособленное подразделение',
                                                                 array(
                                                                     'Название'                          => array(
                                                                         true,
                                                                         ''
                                                                     ),
                                                                     'Адрес'                             => array(
                                                                         true,
                                                                         'address'
                                                                     ),
                                                                     'Фамилия руководителя'              => array(
                                                                         true,
                                                                         ''
                                                                     ),
                                                                     'Имя руководителя'                  => array(
                                                                         true,
                                                                         ''
                                                                     ),
                                                                     'Отчество руководителя'             => array(
                                                                         true,
                                                                         ''
                                                                     ),
                                                                     'Должность'                         => array(
                                                                         true,
                                                                         ''
                                                                     ),
                                                                     'Основание полномочий руководителя' => array(
                                                                         true,
                                                                         ''
                                                                     ),
                                                                     'Номер документа-основания'         => array(
                                                                         false,
                                                                         ''
                                                                     ),
                                                                     'Дата документа-основания'          => array(
                                                                         false,
                                                                         'date'
                                                                     ),
                                                                     'Дата начала полномочий'            => array(
                                                                         true,
                                                                         'date'
                                                                     ),
                                                                     'Дата окончания полномочий'         => array(
                                                                         true,
                                                                         'date'
                                                                     )
                                                                 )
                                        ) ?>
                                        <a class="add" href="javascript:void(0);">Добавить обособленное
                                            подразделение</a>
                                    </article>
                                </div>
                            </section>
                            <?}?>
                            <!--/Обособленные подразделения-->

                            <!--Уполномоченные лица-->
                            <?if($arResult['ELEMENT_PROPERTIES']['142'][0]['VALUE_ENUM'] == 'да'){?>
                            <div class="btnWrp">
                                <a class="add" href="javascript:void(0);"><span>Указать</span> представителей по доверенности</a>
                            </div>
                            <section class="step" data-accordion>
                                <h5 class="none">application step</h5>
                                <a href="javascript:void(0);" data-control>Представители по доверенности</a>
                                <span class="progress incomplete">Заполнено <span>0</span>%</span>

                                <div class="content" data-content>
                                    <article class="wrapper noselect">
                                        <h5 class="none">inner of the app step</h5>
                                        <? CreateCompositeInputs(52, $arResult, $arParams, 'Предстаавитель',
                                                                 array(
                                                                     'Фамилия'  => array(true, ''),
                                                                     'Имя'      => array(true, ''),
                                                                     'Отчество' => array(true, ''),
                                                                     'Должность' => array(true, ''),
                                                                     'Номер доверенности' => array(true, ''),
                                                                     'Дата доверенности' => array(true, 'date'),
                                                                     'Дата начала полномочий' => array(true, 'date'),
                                                                     'Дата окончания полномочий' => array(true, 'date'),
                                                                 )
                                        ) ?>
                                        <a class="add" href="javascript:void(0);">Добавить уполномоченное лицо</a>
                                    </article>
                                </div>
                            </section>
                            <?}?>
                            <!--/Уполномоченные лица-->

                            <section class="laststep">
                                <h5 class="bold">Анкету заполнил</h5>
                                <span class="progress" style="display: none">Заполнено <span>0</span>%</span>
                                <? CreateInput(55, $arResult, $arParams) ?>
                                <? CreateInput(56, $arResult, $arParams) ?>
                                <? CreateInput(57, $arResult, $arParams) ?>
                                <? CreateInput(58, $arResult, $arParams) ?>
                            </section>

                            <div class="notice center">
                                Все изменения анкеты сохранены<br>Заполните все необходимые поля для отправки
                            </div>

                            <div class="center noselect">
                                <input type="submit" name="iblock_submit"
                                       value="Отправить анкету" disabled="disabled" />
                            </div>
                        </section>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>