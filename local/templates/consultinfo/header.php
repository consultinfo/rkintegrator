<?
    if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
    IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/" . SITE_TEMPLATE_ID . "/header.php");
    $CurPage = $APPLICATION->GetCurPage(false);
    preg_match("/\/([^\/]+)\/([^\/]*)\/?([^\?]*)/", $CurPage, $arCurPage);
    define('HOME_PAGE', $CurPage === '/');
    define('APP_PAGE', $arCurPage[1] === 'questionnaire');
    define('GOODS_PAGES', $arCurPage[1] === 'goods');
    define('PRAYS', $arCurPage[1] === 'prays-list');
    define('ART', $arCurPage[1] === 'art');
    define('SERVICE_PAGES', $arCurPage[1] === 'service');
    define('NEWS_DETAIL', ($arCurPage[1] === 'news' && $arCurPage[2] !== '' && $arCurPage[3] !== ''));
    define('ANALYTICS_DETAIL', ($arCurPage[1] === 'analytics' && $arCurPage[2] !== ''));
    define('ANALYTICS_PAGES', $arCurPage[1] === 'analytics');
    define('NEWS_PAGES', $arCurPage[1] === 'news');
    define('ABOUT', $arCurPage[1] === 'about' && $arCurPage[2] === 'company');
    /*
     * Выводить услуги в футуре и на главной странице только для покупателей
     */
    $GLOBALS["Filter_Services"]["PROPERTY_SERVICES_FOR"] = 17;
?>
    <!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="mailru-verification" content="fcf2b7e24b6f82ce" />
        <link rel="shortcut icon" type="image/x-icon" href=""/>
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.easing.1.3.min.js"></script>
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery-ui-1.11.4.js"></script>
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <? CAjax::Init(); ?>
        <?
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/normalize.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/fakeLoader.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/grid.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/slimmenu.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/animated.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/animation.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/tabby.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/accordion.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/formstyler.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/owl.carousel.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/lightbox.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/responsive.css', true);

        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/prefixfree.min.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/modernizr.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/fakeLoader.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/animate.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/parallax.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/scrollto.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/slimmenu.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/owl.carousel.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/tabby.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/modal.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/accordion.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/formstyler.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/waypoints.min.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/lightbox.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/callback.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.maskedinput.js');

        $APPLICATION->ShowHead();
        $APPLICATION->ShowMeta("og:image");
        $APPLICATION->ShowMeta("og:title");
        $APPLICATION->ShowMeta("og:description");?>
        <title>РК Интегратор  |  <? $APPLICATION->ShowTitle(); ?></title>
		<meta name="robots" content="noyaca"/> 
<meta name="robots" content="noodp"/>
    </head>
<body>
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
    <!-- website preloader -->
    <div id="preloader"></div>
    <!-- website preloader -->
    <div id="top"></div>
    <!-- h5 class "none" is used for HTML validation only -->
    <header class="header noselect">
        <h5 class="none"><?=GetMessage("HEADER_TITLE")?></h5>

        <div class="container">
            <div class="line">
                <div class="logotype">
                    <a href="javascript:void(0);" class="unniversary"></a>
                    <a class="block" href="<?=SITE_DIR?>">
                        <img class="responsiveAlt pc" src="<?=SITE_TEMPLATE_PATH?>/images/header/logo_12.png" alt="img">
                        <img class="responsiveAlt mobile" src="<?=SITE_TEMPLATE_PATH?>/images/header/logo_mobile.png"
                             alt="img">
                    </a>
                </div>
                <? $APPLICATION->IncludeComponent(
                    'bitrix:menu',
                    "nav_header",
                    array(
                        "ROOT_MENU_TYPE"        => "nav",
                        "CHILD_MENU_TYPE"       => "child",
                        "MENU_CACHE_TYPE"       => "Y",
                        "MENU_CACHE_TIME"       => "36000000",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS"   => array(),
                        "MAX_LEVEL"             => "2",
                        "USE_EXT"               => "N",
                        "ALLOW_MULTI_SELECT"    => "N"
                    )
                ); ?>
                <div class="profile">
                    <?/* $APPLICATION->IncludeComponent("bitrix:main.include", "", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH"           => SITE_DIR . "include/profile.php"
                    ), false
                    ); */?>
                </div>
            </div>
        </div>
    </header>
<? if (!HOME_PAGE && ERROR_404 != "Y" && !APP_PAGE) { ?>
    <main class="workArea">
    <? $APPLICATION->IncludeComponent(
        "bitrix:breadcrumb", "",
        Array(
            "START_FROM" => "0",
            "PATH"       => "",
            "SITE_ID"    => "s1"
        )
    ); ?>
    <? if (!NEWS_DETAIL && !ANALYTICS_DETAIL && !PRAYS && !ART) { ?>
        <div class="container pt30">
            <?/*if(ABOUT){?>
            <div class="full_image" style="background-image: url(/bitrix/templates/consultinfo/images/content/company/full.jpg);">
                <div class="motto">
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "/about/company/include/title.php"), false); ?>
                </div>
            </div>
            <?}*/?>
            <div class="line clearFix relative">
                <h3><? $APPLICATION->ShowTitle(false); ?></h3>
                <?// $APPLICATION->ShowProperty("TABS_BLOCK"); //Переключатель на странице услуг?>
                <? /*if (GOODS_PAGES || SERVICE_PAGES) { ?>
                    <div class="appWrapper">
                        <a class="btn link" href="#feedback">Оставить заявку</a>
                    </div>
                <? } */?>
            </div>
        </div>
    <? } ?>
<? } ?>