<? if (ERROR_404 != "Y" && !APP_PAGE && !PRAYS && !ART): ?>
    <section id="feedback" class="feedback">
        <div class="container">
            <div class="line">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:form.result.new",
                    "feedback",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "IGNORE_CUSTOM_TEMPLATE" => "Y",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "CACHE_TYPE"             => "A",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        "SEF_FOLDER"             => "",
                        "VARIABLE_ALIASES"       => Array(),
                        "AJAX_MODE"              => "Y",  // режим AJAX
                        "AJAX_OPTION_SHADOW"     => "N", // затемнять область
                        "AJAX_OPTION_JUMP"       => "Y", // скроллить страницу до компонента
                        "AJAX_OPTION_STYLE"      => "N", // подключать стили
                        "AJAX_OPTION_HISTORY"    => "N",
                    )
                ); ?>

                <? $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "main_spectrum",
                    Array(
                        "IBLOCK_TYPE"                     => "content",
                        "IBLOCK_ID"                       => "4",
                        "NEWS_COUNT"                      => "",
                        "SORT_BY1"                        => "SORT",
                        "SORT_ORDER1"                     => "ASC",
                        "SORT_BY2"                        => "ACTIVE_FROM",
                        "SORT_ORDER2"                     => "DESC",
                        "FILTER_NAME"                     => "Filter_Services",
                        "FIELD_CODE"                      => array("", "", ""),
                        "PROPERTY_CODE"                   => array("SERVICES_CLASS", "", ""),
                        "CHECK_DATES"                     => "Y",
                        "DETAIL_URL"                      => "",
                        "AJAX_MODE"                       => "N",
                        "AJAX_OPTION_JUMP"                => "N",
                        "AJAX_OPTION_STYLE"               => "Y",
                        "AJAX_OPTION_HISTORY"             => "N",
                        "CACHE_TYPE"                      => "A",
                        "CACHE_TIME"                      => "36000000",
                        "CACHE_FILTER"                    => "N",
                        "CACHE_GROUPS"                    => "Y",
                        "PREVIEW_TRUNCATE_LEN"            => "",
                        "ACTIVE_DATE_FORMAT"              => "d.m.Y",
                        "SET_TITLE"                       => "N",
                        "SET_BROWSER_TITLE"               => "N",
                        "SET_META_KEYWORDS"               => "N",
                        "SET_META_DESCRIPTION"            => "N",
                        "SET_STATUS_404"                  => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
                        "ADD_SECTIONS_CHAIN"              => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL"        => "N",
                        "PARENT_SECTION"                  => "",
                        "PARENT_SECTION_CODE"             => "",
                        "INCLUDE_SUBSECTIONS"             => "N",
                        "DISPLAY_DATE"                    => "N",
                        "DISPLAY_NAME"                    => "Y",
                        "DISPLAY_PICTURE"                 => "N",
                        "DISPLAY_PREVIEW_TEXT"            => "Y",
                        "PAGER_TEMPLATE"                  => ".default",
                        "DISPLAY_TOP_PAGER"               => "N",
                        "DISPLAY_BOTTOM_PAGER"            => "N",
                        "PAGER_TITLE"                     => "",
                        "PAGER_SHOW_ALWAYS"               => "N",
                        "PAGER_DESC_NUMBERING"            => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL"                  => "N"
                    )
                ); ?>

            </div>
        </div>
    </section>
<? endif; ?>