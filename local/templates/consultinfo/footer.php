<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<? if (ERROR_404 != "Y" && !APP_PAGE && !HOME_PAGE) echo '</main>';?>

<footer class="footer">
    <div class="container">
        <div class="line">
            <div class="contacts">
                <h4 class="alt"><?=GetMessage("FOOTER_CONTACTS_TITLE")?></h4>

                <div class="unit">
                    <span
                        class="block">Адрес: <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/footer_address1.php"), false); ?></span>
                    <span
                        class="block"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/footer_address2.php"), false); ?></span>
                    <span
                        class="block"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/footer_address3.php"), false); ?></span>
                </div>
                <div class="unit">
						<span class="block">
							Телефон/факс: <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/phone.php"), false); ?>
						</span>
                </div>
                <div class="unit">
						<span class="block">
							Адрес электронной почты: <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/email.php"), false); ?>
						</span>
                </div>
            </div>

            <? $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "footer_news",
                Array(
                    "IBLOCK_TYPE"                     => "news",
                    "IBLOCK_ID"                       => "6",
                    "NEWS_COUNT"                      => "3",
                    "SORT_BY1"                        => "SORT",
                    "SORT_ORDER1"                     => "ASC",
                    "SORT_BY2"                        => "ACTIVE_FROM",
                    "SORT_ORDER2"                     => "DESC",
                    "FILTER_NAME"                     => "",
                    "FIELD_CODE"                      => array("", "", ""),
                    "PROPERTY_CODE"                   => array("SERVICES_CLASS", "", ""),
                    "CHECK_DATES"                     => "Y",
                    "DETAIL_URL"                      => "",
                    "AJAX_MODE"                       => "N",
                    "AJAX_OPTION_JUMP"                => "N",
                    "AJAX_OPTION_STYLE"               => "Y",
                    "AJAX_OPTION_HISTORY"             => "N",
                    "CACHE_TYPE"                      => "A",
                    "CACHE_TIME"                      => "36000000",
                    "CACHE_FILTER"                    => "N",
                    "CACHE_GROUPS"                    => "Y",
                    "PREVIEW_TRUNCATE_LEN"            => "",
                    "ACTIVE_DATE_FORMAT"              => "d M Y, h:i",
                    "SET_TITLE"                       => "N",
                    "SET_BROWSER_TITLE"               => "N",
                    "SET_META_KEYWORDS"               => "N",
                    "SET_META_DESCRIPTION"            => "N",
                    "SET_STATUS_404"                  => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
                    "ADD_SECTIONS_CHAIN"              => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL"        => "N",
                    "PARENT_SECTION"                  => "",
                    "PARENT_SECTION_CODE"             => "",
                    "INCLUDE_SUBSECTIONS"             => "N",
                    "DISPLAY_DATE"                    => "N",
                    "DISPLAY_NAME"                    => "Y",
                    "DISPLAY_PICTURE"                 => "N",
                    "DISPLAY_PREVIEW_TEXT"            => "Y",
                    "PAGER_TEMPLATE"                  => ".default",
                    "DISPLAY_TOP_PAGER"               => "N",
                    "DISPLAY_BOTTOM_PAGER"            => "N",
                    "PAGER_TITLE"                     => "",
                    "PAGER_SHOW_ALWAYS"               => "N",
                    "PAGER_DESC_NUMBERING"            => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL"                  => "N"
                )
            ); ?>



            <div class="divide clearFix">&nbsp;</div>
            <? $APPLICATION->IncludeComponent('bitrix:menu', "nav_footer", array(
                    "ROOT_MENU_TYPE"        => "nav",
                    "MENU_CACHE_TYPE"       => "Y",
                    "MENU_CACHE_TIME"       => "36000000",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS"   => array(),
                    "MAX_LEVEL"             => "1",
                    "USE_EXT"               => "N",
                    "ALLOW_MULTI_SELECT"    => "N"
                )
            ); ?>

            <div class="social">

                <div class="logo">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/symbol_small2.png" alt="" class="responsiveAlt">
                </div>
                <div class="developers">
                    <a href="http://consult-info.ru/" target="_blank">Разработка и поддержка сайта Консалт Инфо</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- scroll top -->
<div id="backToTop"><a href="#top">&nbsp;</a></div>
<!-- /scroll top -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter33476928 = new Ya.Metrika({
                    id:33476928,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/33476928" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65914855-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>