<?
//AddEventHandler("iblock", "OnAfterIBlockSectionAdd", Array("Newsletter", "AddNewsletter"));

class Newsletter
{
    // создаем обработчик события "OnAfterIBlockSectionAdd"
    function AddNewsletter(&$arFields)
    {
        define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
        CModule::IncludeModule("subscribe");
        if($arFields["ID"]>0 && $arFields["IBLOCK_ID"] == 16)
            $rubric = new CRubric;
            $arFields = Array(
                "ACTIVE" => ($arFields["ACTIVE"] <> "Y"? "N":"Y"),
                "NAME" => "Прайс-лист: ".$arFields["NAME"],
                "CODE" => $arFields["CODE"],
                "SORT" => $arFields["SORT"],
                "DESCRIPTION" => $arFields["DESCRIPTION"],
                "LID" => "s1"
            );
        $rubric->Add($arFields);
    }
}