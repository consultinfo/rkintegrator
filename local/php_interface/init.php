<?
    require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/include/McryptCipher.php');

    function declensionDate($date)
    {
        $date = str_replace(
            array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),
            array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'),
            $date
        );

        return $date = str_replace(
            array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'),
            array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'),
            $date
        );
    }

    /**
     * Генерация кода из цифр и букв в нижнем и верхнем регистрах
     *
     * @return string
     */
    function randHardCode()
    {
        $arSymbols = array(
            "a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
            "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9
        );
        $code = '';
        for ($i = 0; $i < 12; $i++) {
            $a = rand(0, 57);
            $code = $code . $arSymbols[ $a ];
        }

        return $code;
    }


    /*AddEventHandler("subscribe", "OnBeforeSubscriptionAdd", Array("MySubscribe", "OnBeforeSubscriptionAddHandler"));

    class MySubscribe
    {
        // создаем обработчик события "BeforePostingSendMail"
        function OnBeforeSubscriptionAddHandler(&$arFields)
        {
            echo '<pre>';
            print_r($arFields);die;
            return $arFields;
        }
    }*/

    AddEventHandler("form", "onBeforeResultAdd", Array("MyForm", "onBeforeResultAddHandler"));

    class MyForm
    {
        /*
         * Создаем элемент инфоблока с заявками на анкеты из формы обратной связи
         */
        function onBeforeResultAddHandler($WEB_FORM_ID, $arFields, $arrVALUES)
        {
            if ($WEB_FORM_ID == 1) {
                CModule::IncludeModule("iblock");
                $el = new CIBlockElement;
                $arLoadProductArray = Array(
                    "IBLOCK_ID"         => 1,
                    "IBLOCK_SECTION_ID" => false,
                    "ACTIVE"            => "N",
                    "NAME"              => $arrVALUES[ 'form_text_1' ],
                    "PROPERTY_VALUES"   => array(
                        "EMAIL"   => $arrVALUES[ 'form_email_2' ],
                        "PHONE"   => $arrVALUES[ 'form_text_3' ],
                        "COMMENT" => $arrVALUES[ 'form_textarea_4' ]
                    )
                );
                $el->Add($arLoadProductArray);
            }
        }
    }

    AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("MyIBlock", "OnBeforeIBlockElementUpdateHandler"));
    AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("MyIBlock", "OnBeforeIBlockElementAddHandler"));
    AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("MyIBlock", "OnAfterIBlockElementAddHandler"));
    AddEventHandler("iblock", "OnBeforeIBlockElementDelete", Array("MyIBlock", "OnBeforeIBlockElementDeleteHandler"));

    AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("MyIBlock", "SendNewsOnATS"));
    AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("MyIBlock", "SendNewsOnATS"));
    function createUserForApp($arFields)
    {
        $arPropID = array(81 => "EMAIL", 82 => "PHONE", 83 => "COMMENT", 84 => "TYPE_APP");
        $arProp = array();
        foreach ($arFields[ "PROPERTY_VALUES" ] as $keyProperty => $arProperty) {
            if(is_array($arProperty))
            {
                foreach ($arProperty as $elProperty) {
                    if($elProperty[ "VALUE" ] == 25)
                        $elProperty[ "VALUE" ] = 'llc';
                    elseif($elProperty[ "VALUE" ] == 26)
                        $elProperty[ "VALUE" ] = 'ie';
                    $arProp[ $arPropID[ $keyProperty ] ] = $elProperty[ "VALUE" ];
                }
            }
            else
            {
                if($keyProperty == 84)
                {
                    $arProperty = ($arProperty == 25) ? 'llc' : 'ie';
                }
                $arProp[ $arPropID[ $keyProperty ] ] = $arProperty;
            }
        }

        // Создаем пользователя и отправляем e-mail
        $user = new CUser;
        $password = randHardCode();
        $userFields = Array(
            "NAME"             => $arFields[ "NAME" ],
            "EMAIL"            => $arProp[ "EMAIL" ],
            "PERSONAL_PHONE"   => $arProp[ "PHONE" ],
            "LOGIN"            => "anketa_".$arProp[ "EMAIL" ],
            "LID"              => "ru",
            "ACTIVE"           => "Y",
            "GROUP_ID"         => array(6),
            "PASSWORD"         => $password,
            "CONFIRM_PASSWORD" => $password
        );
        $ID = $user->Add($userFields);

        global $APPLICATION;
        //if (intval($ID) > 0){
            $c = new McryptCipher('KeYfOrPasS');
            $encrypted = urlencode($c->encrypt($userFields['LOGIN']));
            $link = "http://{$_SERVER["SERVER_NAME"]}/questionnaire/?app={$arProp["TYPE_APP"]}&key=$encrypted";
            $fields = array('LINK' => $link, 'EMAIL' => $arProp[ "EMAIL" ], 'USER_NAME' => $userFields["NAME"]);
            CEvent::Send('NEW_USER_FOR_APP','s1',$fields,"Y");
        //}
        /*else {
            $APPLICATION->throwException($user->LAST_ERROR);
            return false;
        }*/

        return true;
    }


    //   Возвращает ID вопроса, по ID раздела, и для работы обязателен ID инфоблока
    function getQuestionIdBySectionId($section_id, $iblock_id){
        $rsSection = CIBlockSection::GetList(
            Array("SORT"=>"ASC"),
            Array(
                "IBLOCK_ID"=>$iblock_id,
                "ID"=>$section_id
            ),
            false,
            Array("UF_VOTE_ID")
        )->GetNext();

        return  $rsSection["UF_VOTE_ID"];
    }

    class MyIBlock
    {
        function SendNewsOnATS(&$arFields) {
            if ($arFields["IBLOCK_ID"] != 8)
                return $arFields;

            $arSelect = array("PREVIEW_PICTURE", "DETAIL_PICTURE", "ID", "XML_ID", "IBLOCK_ID", "CREATED_DATE", "TIMESTAMP_X", "DATE_CREATE", "ACTIVE", "ACTIVE_FROM", "DATE_ACTIVE_FROM", "SORT", "NAME", "PREVIEW_TEXT", "PREVIEW_TEXT_TYPE", "DETAIL_TEXT", "DETAIL_TEXT_TYPE");
            $arFilter = array("ID" => $arFields["ID"]);
            $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
            while($ob = $res->GetNextElement())
            {
                $arSend = $ob->GetFields();
                foreach ($arSend as $code => $field)
                {
                    if(strripos($code, "~") !== false)
                        unset($arSend[$code]);
                }
                $arProps = $ob->GetProperties();
                $arSend["PREVIEW_PICTURE"] = $arSend["PREVIEW_PICTURE"] ? 'http://'.$_SERVER["SERVER_NAME"].CFile::GetPath($arSend["PREVIEW_PICTURE"]) : false;
                $arSend["DETAIL_PICTURE"] = $arSend["DETAIL_PICTURE"] ? 'http://'.$_SERVER["SERVER_NAME"].CFile::GetPath($arSend["DETAIL_PICTURE"]) : false;
                foreach ($arProps as $prop)
                {
                    switch ($prop["PROPERTY_TYPE"])
                    {
                        case "L":
                            $arSend["PROPERTY_VALUES"][ $prop["CODE"] ] = $prop["VALUE_XML_ID"];
                            break;

                        case "F":
                            $arSend["PROPERTY_VALUES"][ $prop["CODE"] ]["n0"]["VALUE"] = $prop["VALUE"]? 'http://'.$_SERVER["SERVER_NAME"].CFile::GetPath($prop["VALUE"]):false;
                            break;

                        default:
                            if ($prop["MULTIPLE"] == "Y")
                            {
                                foreach ($prop["VALUE"] as $key => $val)
                                {
                                    $arSend["PROPERTY_VALUES"][ $prop["CODE"] ]["n".$key] = array("VALUE" => $val, "DESCRIPTION" => $prop["DESCRIPTION"][$key]);
                                }
                            }
                            else
                            {
                                $arSend["PROPERTY_VALUES"][ $prop["CODE"] ]["n0"] = array("VALUE" => $prop["VALUE"], "DESCRIPTION" => $prop["DESCRIPTION"]);;
                            }
                    }
                }
            }

            $url = "http://www.fuelservice.ru/exchange/import_analytics.php";
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, array("arFields" => json_encode($arSend)));
            $res = curl_exec($curl);
            curl_close($curl);

            return $arFields;
        }


        function setAnswer(&$arFields, $UPDATE=false){
            
                CModule::IncludeModule("vote");
                $question = getQuestionIdBySectionId($arFields["IBLOCK_SECTION"][0], $arFields["IBLOCK_ID"]);

                $fields = array(
                    "QUESTION_ID" => $question,
                    "MESSAGE" => $arFields["NAME"],
                    "ACTIVE" => "Y",
                    "FIELD_PARAM" => "id=option".$arFields["ID"]
                );

                if (!$UPDATE ){
                    CVoteAnswer::Add($fields);
                } else {
                    $answer = CVoteAnswer::GetList($question, $by="s_c_sort", $order="asc", $arFilter=array("FIELD_PARAM" => "id=option".$arFields["ID"]))->GetNext();

                    if ($answer){

                        $id = $answer["ID"];
                        CVoteAnswer::Update($id, array("MESSAGE"=>$arFields["NAME"]));
                    } else {
                        CVoteAnswer::Add($fields);
                    }
                }

        }

        function OnAfterIBlockElementAddHandler(&$arFields)
        {
            if (
                $arFields[ "IBLOCK_ID" ] == 1 &&
                $arFields[ "ACTIVE" ] == 'Y' &&
                $arFields[ "PROPERTY_VALUES" ][81] != '' &&
                $arFields[ "PROPERTY_VALUES" ][82] != '' &&
                $arFields[ "PROPERTY_VALUES" ][83] != '' &&
                $arFields[ "PROPERTY_VALUES" ][84] != ''
            )
            {
                createUserForApp($arFields);
            }

            if ($arFields["IBLOCK_ID"] == 17) MyIBlock::setAnswer($arFields);
        }
        /*
         * Если элемент делают активным и указывают тип анкеты, то создаем нового пользователя для этой анкеты и отправляем писльмо на почту клиента со ссылкой для заполнения анкеты
         */
        function OnBeforeIBlockElementUpdateHandler($arFields)
        {
            if ($arFields[ "IBLOCK_ID" ] == 1 && $arFields[ "ACTIVE" ] == 'Y' && $arFields[ "PROPERTY_VALUES" ][84][0]["VALUE"] != '') {
                createUserForApp($arFields);
            }


            if ($arFields["IBLOCK_ID"] == 17) MyIBlock::setAnswer($arFields, true);

            return $arFields;
        }


        function OnBeforeIBlockElementAddHandler(&$arFields)
        {
            if ($arFields[ "IBLOCK_ID" ] == 14) {
                global $APPLICATION;
                $APPLICATION->throwException("Одного элемента достаточно");
                return false;
            }
        }


        function OnBeforeIBlockElementDeleteHandler($ID)
        {
            if ($ID == 123) {
                global $APPLICATION;
                $APPLICATION->throwException("Этот элемент нельзя удалить");
                return false;
            }

            //Необходимо получить элемент инфоблока, чтобы проверить условие ниже
            $el = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("ID"=>$ID))->GetNext();

            if ($el["IBLOCK_ID"]==17){
                CModule::IncludeModule("vote");
                $answer = CVoteAnswer::GetList(
                    getQuestionIdBySectionId($el["IBLOCK_SECTION_ID"], $el["IBLOCK_ID"]),
                    $by="s_c_sort", $order="asc", $arFilter=array("FIELD_PARAM" => "id=option".$ID))->GetNext();

                CVoteAnswer::Delete($answer["ID"]);
            }
        }
    }




    /*Для отписки от рассылки*/
    AddEventHandler("subscribe", "BeforePostingSendMail", array("SubscribeHandlers", "BeforePostingSendMailHandler"));
    class SubscribeHandlers
    {
        function BeforePostingSendMailHandler($arFields)
        {
            CModule::IncludeModule("subscribe");
            $rsSub = CSubscription::GetByEmail($arFields["EMAIL"]);
            $arSub = $rsSub->Fetch();

            $arFields["BODY"] = str_replace("#MAIL_ID#", $arSub["ID"], $arFields["BODY"]);
            $arFields["BODY"] = str_replace("#MAIL_MD5#", SubscribeHandlers::GetMailHash($arFields["EMAIL"]), $arFields["BODY"]);

            return $arFields;
        }

        function GetMailHash($email)
        {
            return md5(md5($email) . 'ProstoStroka');
        }
    }
// Подключить обработчики событий главного модуля
include('include/events/main.php');