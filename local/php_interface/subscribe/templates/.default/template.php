<?
    if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
    global $SUBSCRIBE_TEMPLATE_RUBRIC;
    $SUBSCRIBE_TEMPLATE_RUBRIC = $arRubric;
    global $APPLICATION;
    $_SERVER["SERVER_NAME"] = 'www.fuelbroker.ru';
?>
<div id="mailsub" style="width:100%; padding:0;">
    <center>
        <!-- header -->
        <table border="0" cellpadding="0" cellspacing="0"
               style="width:800px; margin-top: 0; margin-right: auto; margin-bottom: 0; margin-left: auto; background: #e7eff3;">
            <tr>
                <td>
                    <center>
                        <table border="0" cellpadding="0" cellspacing="0"
                               style="width:600px; margin-top: 0; margin-right: auto; margin-bottom: 0; margin-left: auto; border:0;">
                            <tr>
                                <td colspan="2" style="width:100%; height:30px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="width:50%">
                                    <a href="http://www.fuelbroker.ru/" target="_blank"
                                       style="display: block; width:auto; text-decoration:none;">
                                        <img src="http://<?=$_SERVER["SERVER_NAME"]?>/include/subscribe/logo.png"
                                             width="196" height="40" border="0" alt=""
                                             style="display: block; width:auto; text-decoration:none; border:0;">
                                    </a>
                                </td>
                                <td style="width:50%; text-align:right">
                                    <img src="http://<?=$_SERVER["SERVER_NAME"]?>/include/subscribe/tel.png" width="147"
                                         height="16" border="0" alt="phone"
                                         style="display: inline-block; width:auto; text-decoration:none; border:0;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="width:100%; height:30px">&nbsp;</td>
                            </tr>
                        </table>
                    </center>
                </td>
            </tr>
        </table>
        <?
            $APPLICATION->IncludeComponent(
            "bitrix:subscribe.news",
            $arRubric["CODE"],
            array(
                "SITE_ID"            => "s1",
                "IBLOCK_TYPE"        => "news",
                "ID"                 => ($arRubric["CODE"] == 'analytics') ? 8 : 6,
                "SORT_BY"            => "ACTIVE_FROM",
                "SORT_ORDER"         => "DESC",
            ),
            false); ?>
        <!-- features block -->
        <table border="0" cellpadding="0" cellspacing="0"
               style="width:800px; margin:0 auto; padding: 0; color: #344e5a; font-family: Arial; background: #c4d8e1; border:0;">
            <tr>
                <td rowspan="5" style="width:30px;">&nbsp;</td>
                <td style="height:25px;">&nbsp;</td>
                <td rowspan="5" style="width:30px;">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center; background: #c4d8e1;">
                    <h2 style="font-size: 28px; margin: 0; font-weight: normal;">Алгоритм Топливный Интегратор</h2>
                </td>
            </tr>
            <tr>
                <td style="height:10px;">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center; background: #c4d8e1;">
                    <span style="display: block; width:100%; font-size: 16px; margin: 0 0; line-height:23px">
                        Брокерское обслуживание потребителей и поставщиков нефти, нефтепродуктов, химической продукции и природного газа на товарно-сырьевых биржах России
                    </span>
                </td>
            </tr>
            <tr>
                <td style="height:25px;">&nbsp;</td>
            </tr>
        </table>
        <!-- footer -->
        <table border="0" cellpadding="0" cellspacing="0"
               style="width:600px; margin-top: 0; margin-right: auto; margin-bottom: 0; margin-left: auto; color: #344e5a;  font-family: Arial; border:0;">
            <tr>
                <td style="width:100%; height:20px">&nbsp;</td>
            </tr>
            <!-- social network buttons -->
            <tr>
                <td style="text-align:center;">
                    <a href="https://www.facebook.com/fuelbroker" target="_blank"
                       style="display:inline-block; width: 40px; height: 40px; margin: 0 10px 0 0; text-decoration:none; vertical-align: top;">
                        <img src="http://<?=$_SERVER["SERVER_NAME"]?>/include/subscribe/fb.jpg" width="40" height="40"
                             alt="fb" border="0" style="width:40px; height:40px; border:0; text-decoration:none;">
                    </a>
                    <a href="http://vk.com/algorithm.toplivny.integrator" target="_blank"
                       style="display:inline-block; width: 40px; height: 40px; margin: 0 10px 0 0; text-decoration:none; vertical-align: top;">
                        <img src="http://<?=$_SERVER["SERVER_NAME"]?>/include/subscribe/vk.jpg" width="40" height="40"
                             alt="vk" border="0" style="width:40px; height:40px; border:0; text-decoration:none;">
                    </a>
                    <a href="http://www.linkedin.com/company/алгоритм-топливный-интегратор" target="_blank"
                       style="display:inline-block; width: 40px; height: 40px; margin: 0 10px 0 0; text-decoration:none; vertical-align: top;">
                        <img src="http://<?=$_SERVER["SERVER_NAME"]?>/include/subscribe/in.jpg" width="40" height="40"
                             alt="in" border="0" style="width:40px; height:40px; border:0; text-decoration:none;">
                    </a>
                    <a href="http://www.youtube.com/channel/UCOuTh0NfKc_9YWHnj78dj1w " target="_blank"
                       style="display:inline-block; width: 40px; height: 40px; margin: 0; text-decoration:none; vertical-align: top;">
                        <img src="http://<?=$_SERVER["SERVER_NAME"]?>/include/subscribe/youtube.jpg" width="40" height="40"
                             alt="in" border="0" style="width:40px; height:40px; border:0; text-decoration:none;">
                    </a>
                </td>
            </tr>
            <tr>
                <td style="width:100%; height:20px">&nbsp;</td>
            </tr>
            <tr>
                <td style="width:100%; text-align:center;">
                    <span style="display: block; width: 100%; margin:0 0 5px 0; font-size: 13px;">Общество с ограниченной ответственностью «Алгоритм Топливный Интегратор»</span>
                </td>
            </tr>
            <tr>
                <td style="width:100%; text-align:center;">
                    <span style="display: block; width: 100%; margin:0 0 5px 0; font-size: 13px;">Адрес: 125167 Россия, Москва, Ленинградский проспект, 47/2 (БЦ «Авион»), подъезд 2, этаж 3.</span>
                </td>
            </tr>
            <tr>
                <td style="width:100%; text-align:center;">
                    <span
                        style="display: block; width: 100%; margin:0 0 5px 0; font-size: 13px;">+7 (495) 780-97-00 | <a
                            href="mailto:info@fuelbroker.ru" target="_blank"
                            style="color: #344e5a; text-decoration: none">info@fuelbroker.ru</a></span>
                </td>
            </tr>
            <tr>
                <td style="width:100%; height:5px">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align:center;">
                    <span style="display: block; width: 100%; margin:0 0 5px 0; font-size: 13px;">Вы получили это письмо, т.к. являетесь клиентом компании «Алгоритм Топливный Интегратор»</span>
                </td>
            </tr>
            <tr>
                <td style="width:100%; text-align:center;">
                    <span style="display: block; width: 100%; margin:0 0 5px 0; font-size: 13px;"> и/или подписались на рассылку на <a
                            href="http://www.fuelbroker.ru/" target="_blank"
                            style="color: #344e5a; text-decoration: underline">fuelbroker.ru</a> или на <a
                            href="http://www.fuelservice.ru/" target="_blank"
                            style="color: #344e5a; text-decoration: underline">fuelservice.ru</a></span>
                </td>
            </tr>
            <tr>
                <td style="width:100%; text-align:center;">
                    <span style="display: block; width: 100%; margin:0 0 5px 0; font-size: 13px;">
                        <a href="http://www.fuelbroker.ru/unsubscribe.php?mid=#MAIL_ID#&mhash=#MAIL_MD5#" target="_blank" style="color: #344e5a; text-decoration: underline">отписаться</a>
                    </span>
                </td>
            </tr>
            <tr>
                <td style="width:100%; height:5px">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align:center;">
                    <span style="display: block; width: 100%; margin:0 0 5px 0; font-size: 13px; color: #781E32">При перепечатке ссылка на компанию "Алгоритм Топливный Интегратор" обязательна.</span>
                </td>
            </tr>
            <tr>
                <td style="width:100%; height:20px">&nbsp;</td>
            </tr>
            <!-- footer logotype -->
            <tr>
                <td style="text-align:center;">
                    <a href="http://www.fuelbroker.ru/" target="_blank"
                       style="display: inline-block; width:auto; text-decoration:none;">
                        <img src="http://<?=$_SERVER["SERVER_NAME"]?>/include/subscribe/f_logo.jpg" width="100"
                             height="51" border="0" alt="logotype"
                             style="display: block; width:auto; text-decoration:none; border:0;">
                    </a>
                </td>
            </tr>
            <tr>
                <td style="width:100%; height:20px">&nbsp;</td>
            </tr>
        </table>
    </center>
</div>

<?
    $new_date = $DB->FormatDate(date("d.m.Y H:i:s"), "DD.MM.YYYY HH:MI:SS", CSite::GetDateFormat("FULL", "ru"));
    //if ($SUBSCRIBE_TEMPLATE_RESULT)
    return array(
        "ID"          => $SUBSCRIBE_TEMPLATE_RUBRIC["ID"],
        "SUBJECT"     => $SUBSCRIBE_TEMPLATE_RUBRIC["NAME"],
        "BODY_TYPE"   => "html",
        "CHARSET"     => "UTF-8",
        "DIRECT_SEND" => "Y",
        "FROM_FIELD"  => $SUBSCRIBE_TEMPLATE_RUBRIC["FROM_FIELD"]
        //"AUTO_SEND_TIME" => $new_date
    );
    /*else
        return false;*/
?> 