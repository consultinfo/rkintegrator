<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
    $name = "";
    if(ANALYTICS_PAGES)
        $name = "title_analytics";
    else if(NEWS_PAGES)
        $name = "title_news";

?>

<div class="closeWrap"><span class="b-close">&nbsp;</span></div>
<header class="popup">
    <h4 class="center"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "/include/subscribe/$name.php"), false); ?></h4>
</header>
<section class="popup">
    <h5 class="none">form</h5>
    <? /*echo '<pre>';print_r($arResult);*/ ?>
    <form enctype="multipart/form-data" method="POST" action="<?=$_SERVER["REQUEST_URI"]?>" name="SUBSCRIBE_FORM">
        <? foreach ($arResult["REQUEST"] as $key => $elInput) { ?>
            <div class="unit clearFix">
                <div class="label">
                    <span class="cell"><?=$elInput["NAME"]?></span>
                </div>
                <div class="value<? if ($elInput["ERROR"] != '') {
                    echo ' error';
                } ?>">
                    <input type="text" name="<?=$key?>"
                           value="<?=$elInput["VALUE"]?>"/>
                    <? if ($elInput["ERROR"] != '') { ?>
                        <span class="oOps noselect">
                                <span class="inner">
                                    <?=$elInput["ERROR"]?>
                                </span>
                            </span>
                    <? } ?>
                </div>
            </div>
        <? } ?>

        <input type="hidden" name="RUB_ID" value="<?=$arParams["RUB_ID"]?>"/>

        <div class="unit clearFix">
            <div class="label">&nbsp;</div>
            <label class="value triggers">
                <input type="checkbox" name="form_checkbox_eula" id="form_checkbox_eula">
                Я согласен с
                <a href="/eula/" class="eula" target="_blank">Правилами использования сайта и обработку своих персональных данных</a>
            </label>
        </div>

        <div class="unit center">
                <!--<span class="iblock prompt">
                    <?/* $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "/include/subscribe/conditions_link.php"), false); */?>
                </span>-->

            <? if ($arResult["FORM_NOTE"] != '') { ?>
                <input type="submit" style="display: none" name="web_form_submit" value="Подписаться"/>
                <div class="notification success">
                    <span class="ico success">&nbsp;</span>
                    <span class="message"><?=$arResult["FORM_NOTE"];?></span>
                </div>
            <? } else if ($arResult["ERROR"]["SUBSCR"] != '') { ?>
                <div class="notification failure">
                    <span class="ico failure">&nbsp;</span>
                    <span class="message"><?=$arResult["ERROR"]["SUBSCR"]?></span>
                </div>
            <?} else {?>
                <input type="submit" name="web_form_submit" value="Подписаться"/>
            <?}?>
        </div>
    </form>


    <script>
        if($('.notification').hasClass('success')){
            $('form[name="SUBSCRIBE_FORM"] input:submit').css('display', 'none');
            setTimeout(function () {
                $('.notification').css('display', 'none');
                $('form[name="SUBSCRIBE_FORM"] input:submit').css({"display":"inline-block"});
            }, 5000);
        }
    </script>
</section>
<script>
    /**
     * @return {string}
     */
    function GetErrorText(input_name) {
        var error_text = '';
        switch (input_name) {
            case 'EMAIL':
                error_text = 'Укажите Ваш email';
                break;
            case 'NAME':
                error_text = 'Представьтесь пожалуйста (фамилия имя отчество)';
                break;
            case 'WORK_COMPANY':
                //error_text = 'Укажите название компании';
                error_text = '';
                break;
            case 'WORK_POSITION':
                //error_text = 'Укажите Вашу должность';
                error_text = '';
                break;
            case 'validate_email':
                error_text = 'Введен некорректный адрес email';
                break;
            case 'eula':
                error_text = 'Необходимо ознакомиться с правилами использования сайта';
                break;
        }

        //return 'error_text';
        return '<span class="oOps noselect"><span class="inner">' + error_text +'</span></span>';
    }
    BX.ready(function () {
        // Правилами использования сайта
        $("#subscribe  #form_checkbox_eula").change(function () {
            if ($(this).is(":checked")) {
                $('form[name="SIMPLE_FORM_1"] input[type=submit]').attr('disabled', false);
            }
        });


        /* Если есть сообщение об ошибке, то убирем его при нажатии на input */
        $('form[name="SUBSCRIBE_FORM"] .value input').on('focus', function () {
            var parent = $(this).parents(".value");
            var error = parent.find(".oOps");
            if (error) {
                parent.removeClass('error');
                error.remove();
            }
        });

        /* Если при переходе на другой input этот не заполнен, то показываем ошибку */
        $('form[name="SUBSCRIBE_FORM"] .value input').on('blur keyup', function () {
            var input_name = $(this).attr('name');
            var parent = $(this).parents(".value");
            var error = false;
            parent.find('.oOps').remove();
            if ((input_name == 'NAME' || input_name == 'EMAIL') && $.trim($(this).val()) == ''/* || (input_name == "NAME" && $.trim($(this).val()).replace(/\s+/g, " ").split(' ').length < 3)*/) {
                parent.addClass('error').append(GetErrorText(input_name));
                error = true;
            } else if(input_name == 'EMAIL' && $.trim($(this).val()) != '') {
                var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
                if(pattern.test($(this).val()) == false){
                    parent.addClass('error').append(GetErrorText('validate_email'));
                    error = true;
                }
            }
            if(error == false){
                parent.removeClass('error');
            }

            $('form[name="SUBSCRIBE_FORM"] input[type=submit]').attr('disabled', true);
            var validate = true;
            $('form[name="SUBSCRIBE_FORM"] .value input').each(function(){
                var p = $(this).parents(".value");
                if(p.hasClass('error')){
                    validate = false;
                    return false;
                } });
            if(validate) {
                $('form[name="SUBSCRIBE_FORM"] input[type=submit]').attr('disabled', false);
            }


        });

        /* Событие отправки */
        $('form[name="SUBSCRIBE_FORM"] input[type="submit"]').on('click', function (event) {
            var validate = true;
            $('form[name="SUBSCRIBE_FORM"] .value input').each(function () {
                var input_name = $(this).attr('name');
                var parent = $(this).parents(".value");
                if ((input_name == 'NAME' || input_name == 'EMAIL') && $.trim($(this).val()) == '') {
                    parent.addClass('error').append(GetErrorText(input_name));
                    validate = false;
                } else if(input_name == 'EMAIL' && $.trim($(this).val()) != '') {
                    var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
                    if(pattern.test($(this).val()) == false){
                        parent.addClass('error').append(GetErrorText('validate_email'));
                        validate = false;
                    }
                }
            });

            // Правилами использования сайта
            if (!$("#subscribe #form_checkbox_eula").is(":checked")) {
                $("#subscribe #form_checkbox_eula").parent().addClass('error').append(GetErrorText('eula'));
                validate = false;
            }

            if (!validate) {
                $('form[name="SUBSCRIBE_FORM"] input[type=submit]').attr('disabled', true);
                event.preventDefault();
            }
        });
    });
</script>