<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"AJAX_MODE" => array(),
		"RUB_ID" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME"=>GetMessage("SUBSCR_RUB_ID"),
			"TYPE"=>"TEXT",
			"DEFAULT"=>"N",
		),
		"SET_TITLE" => array(),
	),
);
?>
