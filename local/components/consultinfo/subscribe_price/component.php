<?
    if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
    /** @global CMain $APPLICATION */
    /** @global CUser $USER */
    /** @global CDatabase $DB */
    /** @var CBitrixComponent $this */
    /** @var array $arParams */
    /** @var array $arResult */
    /** @var string $componentName */
    /** @var string $componentPath */
    /** @var string $componentTemplate */
    /** @var string $parentComponentName */
    /** @var string $parentComponentPath */
    /** @var string $parentComponentTemplate */
    $this->setFrameMode(false);

    if (!CModule::IncludeModule("subscribe")) {
        ShowError(GetMessage("SUBSCR_MODULE_NOT_INSTALLED"));

        return;
    }
    $errMsg = array();
    $arInput = array(
        "EMAIL"         => "Ваш email",
        "NAME"          => "Ф.И.О.",
        "WORK_COMPANY"  => "Компания",
        "WORK_POSITION" => "Должность"
    );

    if ($_REQUEST["web_form_submit"]) {
        $APPLICATION->RestartBuffer();
        foreach ($_REQUEST as $key => $elRequest) {
            if ($elRequest == '' && $key != "web_form_submit") {
                $errMsg["FORM"][ $key ] = 'Не заполнено поле "' . $arInput[ $key ] . '"';
            }
        }

        if (empty($errMsg["FORM"])) {
            /*
             * Проверка Email подпики на существоване
             */
            $rsSubscription = CSubscription::GetByEmail($_REQUEST["EMAIL"]);
            if ($arSubscription = $rsSubscription->Fetch()) {
                $arSubscriptionRub = CSubscription::GetRubricArray($arSubscription["ID"]);
                if(in_array($arParams["RUB_ID"], $arSubscriptionRub)){
                    $arResult["FORM_NOTE"] =  GetMessage("subscr_isset_mess");
                } else {
                    $arSubscriptionRub[] = $arParams["RUB_ID"];
                    $subscr = new CSubscription;
                    if ($subscr->Update(
                        $arSubscription["ID"],
                        array(
                            "SEND_CONFIRM" => "N",
                            "RUB_ID"       => $arSubscriptionRub
                        )
                    )
                    ) {
                        $arResult["FORM_NOTE"] = GetMessage("subscr_update_mess");
                        $url = 'http://www.fuelbroker-mail.ru/wp-admin/admin-ajax.php?action=newsletters_api';
                        $data = array(
                            'api_method' => 'subscriber_add',
                            'api_key' => 'F904C84DD1B3427802E5B2F83B335CE3',
                            'api_data' => array(
                                'email' => $_REQUEST["EMAIL"],
                                'list_id' => array(3),
                            )
                        );

                        $data_string = json_encode($data);

                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                'Content-Type: application/json',
                                'Content-Length: ' . strlen($data_string))
                        );
                        curl_exec($ch);
                        curl_close($ch);
                    }
                    else
                        $errMsg["SUBSCR"] = $subscr->LAST_ERROR;
                }
            } else {
                /*
             * Проверка пользователья на существование
             */
                $rsUser = CUser::GetByLogin($_REQUEST["EMAIL"]);
                if ($arUser = $rsUser->Fetch()) {
                    $USER_ID = $arUser["ID"];
                } else {
                    /*
                     * Создание нового пользователья для подписки
                     */
                    $pass = randHardCode();
                    $user = new CUser;
                    $arFieldsUser = Array(
                        "NAME"             => $_REQUEST["NAME"],
                        "EMAIL"            => $_REQUEST["EMAIL"],
                        "LOGIN"            => $_REQUEST["EMAIL"],
                        "ACTIVE"           => "Y",
                        "GROUP_ID"         => array(5), //группа для для пользователей с подпиской
                        "PASSWORD"         => $pass,
                        "CONFIRM_PASSWORD" => $pass,
                        "WORK_COMPANY"     => $_REQUEST["WORK_COMPANY"],
                        "WORK_POSITION"    => $_REQUEST["WORK_POSITION"],
                    );
                    $USER_ID = $user->Add($arFieldsUser);
                }
                /*
                 * Создание подписки для пользователья
                 */
                $arFieldsSubscr = Array(
                    "USER_ID"   => $USER_ID,
                    "FORMAT"    => "html",
                    "EMAIL"     => $_REQUEST["EMAIL"],
                    "ACTIVE"    => "Y",
                    "RUB_ID"    => array($arParams["RUB_ID"]),
                    "CONFIRMED" => "Y",
                    "SEND_CONFIRM" => "N"
                );
                $subscr = new CSubscription;
                $Subscr_ID = $subscr->Add($arFieldsSubscr);
                if ($Subscr_ID > 0)
                {
                    $arResult["FORM_NOTE"] = GetMessage("subscr_active_mess");

                    $url = 'http://www.fuelbroker-mail.ru/wp-admin/admin-ajax.php?action=newsletters_api';
                    $data = array(
                        'api_method' => 'subscriber_add',
                        'api_key' => 'F904C84DD1B3427802E5B2F83B335CE3',
                        'api_data' => array(
                            'email' => $_REQUEST["EMAIL"],
                            'list_id' => array(3),
                        )
                    );

                    $data_string = json_encode($data);

                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($data_string))
                    );
                    curl_exec($ch);
                    curl_close($ch);
                }
                else
                {
                    $errMsg["SUBSCR"] = $subscr->LAST_ERROR;
                }
            }
        }
    }

    $arResult["ERROR"] = $errMsg;
    foreach ($arInput as $key => $nameInput) {
        $arResult["REQUEST"][ $key ]["NAME"] = $nameInput;
        $arResult["REQUEST"][ $key ]["VALUE"] = htmlspecialcharsbx($_REQUEST[ $key ]);
        $arResult["REQUEST"][ $key ]["ERROR"] = $errMsg["FORM"][ $key ];
    }

    $this->IncludeComponentTemplate();
?>
