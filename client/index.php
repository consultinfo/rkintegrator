<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Выгодная покупка и продажа на товарно-сырьевой бирже. Регламент брокерского обслуживания / Шаблоны документов и другая полезная информация для Вас на нашем сайте.");
$APPLICATION->SetPageProperty("title", "Полезная информация для клиентов | Контактные данные для связи");
    $APPLICATION->SetTitle("Клиентам"); ?>
    <div class="container delivery">
        <div class="line relative clearFix">
            <section class="half">
                <h4><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/title.php"), false); ?></h4>
                <div class="item">
						<span class="important">
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/mail.php"), false); ?>
						</span>
                    <span class="prompt ml15 iblock"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/mail_title.php"), false); ?></span>
                </div>
                <div class="item">
                    <span class="important"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/phone.php"), false); ?></span>
                    <span class="prompt ml15 iblock"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/phone_title.php"), false); ?></span>
                </div>
                <div class="item">
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/btn.php"), false); ?>
                </div>
                <?/*<div class="item">
                    <a rel="request" href="javascript:void(0);" class="btn alt">Заполнить анкету</a>
                </div>*/?>
            </section>
            <section class="half">
                <h4><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/half_title.php"), false); ?></h4>
                <div class="item">
                    <span class="props upp block"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/half1.php"), false); ?></span>
                    <span class="props block upp"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/half2.php"), false); ?></span>
                    <span class="props block"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/half3.php"), false); ?></span>
                    <span class="props block"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/half4.php"), false); ?></span>
                    <span class="props block"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/half5.php"), false); ?></span>
                    <span class="props block"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/half6.php"), false); ?></span>
                    <span class="prompt block"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/half7.php"), false); ?></span>
                </div>
            </section>
        </div>
        <div class="substrate typeOne dark">&nbsp;</div>
        <div class="substrate typeTwo light">&nbsp;</div>
    </div>


    <div class="container">
    <div class="line">
    <section class="samples clearFix">
        <h5 class="none"></h5>
<? $APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "client_samples",
    Array(
        "IBLOCK_TYPE"                     => "content",
        "IBLOCK_ID"                       => "10",
        "NEWS_COUNT"                      => "",
        "SORT_BY1"                        => "SORT",
        "SORT_ORDER1"                     => "ASC",
        "SORT_BY2"                        => "ACTIVE_FROM",
        "SORT_ORDER2"                     => "DESC",
        "FILTER_NAME"                     => "",
        "FIELD_CODE"                      => array("", "", ""),
        "PROPERTY_CODE"                   => array("FILE", "", ""),
        "CHECK_DATES"                     => "Y",
        "DETAIL_URL"                      => "",
        "AJAX_MODE"                       => "N",
        "AJAX_OPTION_JUMP"                => "N",
        "AJAX_OPTION_STYLE"               => "Y",
        "AJAX_OPTION_HISTORY"             => "N",
        "CACHE_TYPE"                      => "A",
        "CACHE_TIME"                      => "36000000",
        "CACHE_FILTER"                    => "N",
        "CACHE_GROUPS"                    => "Y",
        "PREVIEW_TRUNCATE_LEN"            => "",
        "ACTIVE_DATE_FORMAT"              => "d.m.Y",
        "SET_TITLE"                       => "N",
        "SET_BROWSER_TITLE"               => "N",
        "SET_META_KEYWORDS"               => "N",
        "SET_META_DESCRIPTION"            => "N",
        "SET_STATUS_404"                  => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
        "ADD_SECTIONS_CHAIN"              => "N",
        "HIDE_LINK_WHEN_NO_DETAIL"        => "N",
        "PARENT_SECTION"                  => "",
        "PARENT_SECTION_CODE"             => "",
        "INCLUDE_SUBSECTIONS"             => "N",
        "DISPLAY_DATE"                    => "N",
        "DISPLAY_NAME"                    => "Y",
        "DISPLAY_PICTURE"                 => "N",
        "DISPLAY_PREVIEW_TEXT"            => "Y",
        "PAGER_TEMPLATE"                  => ".default",
        "DISPLAY_TOP_PAGER"               => "N",
        "DISPLAY_BOTTOM_PAGER"            => "N",
        "PAGER_TITLE"                     => "",
        "PAGER_SHOW_ALWAYS"               => "N",
        "PAGER_DESC_NUMBERING"            => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL"                  => "N"
    )
); ?>





<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "client_orders",
    Array(
        "IBLOCK_TYPE"                     => "content",
        "IBLOCK_ID"                       => "11",
        "NEWS_COUNT"                      => "",
        "SORT_BY1"                        => "ACTIVE_FROM",
        "SORT_ORDER1"                     => "DESC",
        "SORT_BY2"                        => "SORT",
        "SORT_ORDER2"                     => "DESC",
        "FILTER_NAME"                     => "",
        "FIELD_CODE"                      => array("", "", ""),
        "PROPERTY_CODE"                   => array("FILE", "FLAG_RULES", ""),
        "CHECK_DATES"                     => "Y",
        "DETAIL_URL"                      => "",
        "AJAX_MODE"                       => "N",
        "AJAX_OPTION_JUMP"                => "N",
        "AJAX_OPTION_STYLE"               => "Y",
        "AJAX_OPTION_HISTORY"             => "N",
        "CACHE_TYPE"                      => "A",
        "CACHE_TIME"                      => "36000000",
        "CACHE_FILTER"                    => "N",
        "CACHE_GROUPS"                    => "Y",
        "PREVIEW_TRUNCATE_LEN"            => "",
        "ACTIVE_DATE_FORMAT"              => "d.m.Y",
        "SET_TITLE"                       => "N",
        "SET_BROWSER_TITLE"               => "N",
        "SET_META_KEYWORDS"               => "N",
        "SET_META_DESCRIPTION"            => "N",
        "SET_STATUS_404"                  => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
        "ADD_SECTIONS_CHAIN"              => "N",
        "HIDE_LINK_WHEN_NO_DETAIL"        => "N",
        "PARENT_SECTION"                  => "",
        "PARENT_SECTION_CODE"             => "",
        "INCLUDE_SUBSECTIONS"             => "N",
        "DISPLAY_DATE"                    => "N",
        "DISPLAY_NAME"                    => "Y",
        "DISPLAY_PICTURE"                 => "N",
        "DISPLAY_PREVIEW_TEXT"            => "Y",
        "PAGER_TEMPLATE"                  => ".default",
        "DISPLAY_TOP_PAGER"               => "N",
        "DISPLAY_BOTTOM_PAGER"            => "N",
        "PAGER_TITLE"                     => "",
        "PAGER_SHOW_ALWAYS"               => "N",
        "PAGER_DESC_NUMBERING"            => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL"                  => "N"
    )
);?>
    </section>
    </div>
    </div>


    <div id="request">
        <div class="closeWrap"><span class="b-close">&nbsp;</span></div>

            <?$APPLICATION->IncludeComponent(
                "bitrix:iblock.element.add.form",
                "new_app",
                Array(
                    "COMPONENT_TEMPLATE" => "new_app",
                    "IBLOCK_TYPE" => "questionnaires",
                    "IBLOCK_ID" => "1",
                    "STATUS_NEW" => "N",
                    "LIST_URL" => "",
                    "USE_CAPTCHA" => "N",
                    "USER_MESSAGE_EDIT" => "да",
                    "USER_MESSAGE_ADD" => "На Ваш E-mail отправлено письмо",
                    "DEFAULT_INPUT_SIZE" => "30",
                    "RESIZE_IMAGES" => "N",
                    "PROPERTY_CODES" => array("81", "82", "83", "84", "NAME"),
                    "PROPERTY_CODES_REQUIRED" => array("81", "82", "83", "84", "NAME"),
                    "GROUPS" => array("2"),
                    "STATUS" => "ANY",
                    "ELEMENT_ASSOC" => "CREATED_BY",
                    "MAX_USER_ENTRIES" => "100000",
                    "MAX_LEVELS" => "100000",
                    "LEVEL_LAST" => "Y",
                    "MAX_FILE_SIZE" => "0",
                    "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
                    "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                    "SEF_MODE" => "N",
                    "CUSTOM_TITLE_NAME" => "Ваше имя",
                    "CUSTOM_TITLE_TAGS" => "",
                    "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                    "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                    "CUSTOM_TITLE_IBLOCK_SECTION" => "",
                    "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                    "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                    "CUSTOM_TITLE_DETAIL_TEXT" => "",
                    "CUSTOM_TITLE_DETAIL_PICTURE" => "",
                    "SEF_FOLDER" => "/client/",
                    "VARIABLE_ALIASES" => Array(
                    ),
                "AJAX_MODE" => "Y",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                )
            );?>







    </div>

    <script>
        $('[rel="request"]').bind('click', function(e) {
            e.preventDefault();
            $('#request').bPopup({
                speed: 250,
                transition: 'slideUp'
            });
        });
    </script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>