<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
    CModule::IncludeModule("iblock");
    if (!is_numeric($_GET['id'])) {
        $arSelect = Array("ID", "IBLOCK_ID", "IBLOCK_NAME", "NAME");
        $iblock_filter = array("LOGIC" => "OR", array("IBLOCK_ID" => 2), array("IBLOCK_ID" => 3));
        $arFilter = Array($iblock_filter, 'PROPERTY_APP_STATUS_VALUE' => 'утверждена');
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNextElement()) {
            $el = $arFields = $ob->GetFields();
            if($el['IBLOCK_ID'] == 3){
                $AppIE .= '<a href="?id=' . $el['ID'] . '">' . $el['NAME'] . '</a><br />';
            } else {
                $AppLLC .= '<a href="?id=' . $el['ID'] . '">' . $el['NAME'] . '</a><br />';
            }
        }
        if($AppIE != ''){
            echo '<h1>Анкеты для ИП</h1>';
            echo $AppIE;
        }
        if($AppLLC != ''){
            echo '<h1>Анкеты для ООО</h1>';
            echo $AppLLC;
        }
        if($AppIE == '' && $AppLLC == ''){
            echo 'Анкет не найдено';
        }

    } else {
        $id = $_GET['id'];
        $res = CIBlockElement::GetByID($id);
        if ($obElement = $res->GetNextElement()) {
            $el = $obElement->GetFields();
            $el["PROPERTIES"] = $obElement->GetProperties();
            if ($el["IBLOCK_ID"] != 2 && $el["IBLOCK_ID"] != 3) {
                die('Анкета не найдена');
            }
            if ($el["PROPERTIES"]["APP_STATUS"]["VALUE_XML_ID"] != 'app_true') {
                //die('Анкета еще не утверждена');
            }

            header("content-type: application/xml; charset=UTF-8");
            $arSections = array();
            // Блоки ООО
            $arSections[2] = array(
                'BASE_INFO'              => 'Базовая информация',
                'ADDRESS_LEGAL_PERSON'   => 'Адреса юридического лица',
                'FOUNDERS_INFO'          => 'Собственники и бенефициарные владельцы',
                'AUTHORITY_INFO'         => 'Сведения об органах юридического лица (кроме единоличного исполнительного органа)',
                'AFFILIATES'             => 'Аффилированные лица',
                'ESTATE'                 => 'Величина уставного капитала и стоимость имущества',
                'STRUCTURE_SUBDIVISIONS' => 'Обособленные подразделения',
                'BANK_ACCOUNT_DETAILS'   => 'Реквизиты банковского счета в рублях',
                'AUTHORIZED_PERSONS'     => 'Представители по доверенности',
                'WHO_FILLED'             => 'Анкету заполнил'
            );
            // Блоки ИП
            $arSections[3] = array(
                'BASE_INFO'            => 'Базовая информация',
                'IDENTITY_DOCUMENT'    => 'Реквизиты документа, удостоверяющего личность',
                'MIGRATION_CARD'       => 'Данные миграционной карты',
                'RIGHT_TO_STAY'        => 'Данные документа, подтверждающего право иностранного гражданина или лица без гражданства на пребывание (проживание) в РФ',
                'ADDRESS_PERSON'       => 'Адреса физического лица',
                'REGISTRATION_IE'      => 'Cведения о государственной регистрации физического лица в качестве индивидуального предпринимателя',
                'BANK_ACCOUNT_DETAILS' => 'Реквизиты банковского счета в рублях',
                'AUTHORIZED_PERSONS'   => 'Представители по доверенности',
                'WHO_FILLED'           => 'Анкету заполнил'
            );
            $arProps = array();
            foreach ($el["PROPERTIES"] as $key => $arProperty) {
                if (!empty($arProperty["VALUE"])) {
                    foreach ($arSections[ $el["IBLOCK_ID"] ] as $keySection => $nameSection) {
                        if (mb_stristr($key, $keySection)) {
                            if (is_array($arProperty["VALUE"])) {
                                if ($arProperty["VALUE"][0] == 'Да') {
                                    $arProps[ $nameSection ][ $arProperty["NAME"] ] = 'Да';
                                } else {
                                    foreach ($arProperty["VALUE"] as $keyValue => &$value) {
                                        $arValue = explode("\n", $value);
                                        $tmp = array();
                                        foreach ($arValue as &$elValue) {
                                            $arElValue = explode(': ', $elValue);
                                            $keyCompositeProp = $arElValue[0];
                                            $nameCompositeProp = $arElValue[1];
                                            //$tmp[ $arProperty["NAME"] . ' #' . ($keyValue + 1) ][ $keyCompositeProp ] =
                                            //    $nameCompositeProp;
                                            //$arProperty[ "VALUE" ][$keyValue][$keyCompositeProp] = $nameCompositeProp;
                                            //$value[$keyCompositeProp] = $nameCompositeProp;
                                            $arProps[ $nameSection ][ $arProperty["NAME"] . ' #' . ($keyValue + 1) ][ $keyCompositeProp ] =
                                                $nameCompositeProp;
                                        }
                                    }
                                }
                            } else {
                                 $arProps[ $nameSection ][ $arProperty["NAME"] ] = $arProperty["VALUE"];
                            }
                        }
                    }
                }
            }

            $filename = $_SERVER['DOCUMENT_ROOT'] . '/questionnaire/xml/test.xml';
            $xml = new DomDocument('1.0', 'UTF-8');
            $XMLDoc = $xml->appendChild($xml->createElement('anketa'));
            foreach ($arProps as $keySection => $section) {
                $XMLSection = $xml->createElement('section');
                $XMLSection->setAttribute("name", $keySection);
                $XMLDoc->appendChild($XMLSection);
                foreach ($section as $keyProperty => $property) {
                    if (!is_array($property)) {
                        $XMLProperty = $xml->createElement('property', $property);
                        $XMLProperty->setAttribute("name", $keyProperty);
                    } else {
                        $XMLProperty = $xml->createElement('multiple_property');
                        $XMLProperty->setAttribute("name", $keyProperty);
                        foreach ($property as $keyMultipleProperty => $MultipleProperty) {
                            $XMLMultipleProperty = $xml->createElement('property', $MultipleProperty);
                            $XMLMultipleProperty->setAttribute("name", $keyMultipleProperty);
                            $XMLProperty->appendChild($XMLMultipleProperty);
                        }
                    }
                    $XMLSection->appendChild($XMLProperty);
                }
            }

            print $xml->saveXML();
            //print $xml->save($filename);
        } else {
            echo 'Анкета не найдена';
        }
    }

    require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_after.php');