<?
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

    global $USER;
    if ($USER->IsAuthorized()) {
        CModule::IncludeModule("iblock");
        $arSelect = Array("ID", "IBLOCK_ID", "ACTIVE");

        $iblock_filter = array("LOGIC" => "OR", array("IBLOCK_ID" => 2), array("IBLOCK_ID" => 3));

        $arFilter = Array($iblock_filter, "CREATED_USER_ID" => $USER->GetID());
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $propertyValue = array();
            $propertyID = $_POST['propertyID'];
            $propertyKey = $_POST['propertyKey'];

            if ($propertyKey != '0') {
                $propertyValue = array();
                $res = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields["ID"], array(),
                                                   array("ID" => $propertyID)
                );
                while ($ob = $res->GetNext()) {
                    $propertyValue[]['VALUE'] = $ob['VALUE'];
                }
                $propertyValue[ $propertyKey ]['VALUE'] = $_POST['propertyValue'];
            } else {
                $propertyValue = $_POST['propertyValue'];
            }

            if ($_POST['submit'] == 'complete') {// ООО
                $propertyValue = ($arFields["IBLOCK_ID"] == 2) ? 34 : 38;
                $propertyID = ($arFields["IBLOCK_ID"] == 2) ? 128 : 129;
            }


            CIBlockElement::SetPropertyValuesEx($arFields["ID"], $arFields["IBLOCK_ID"],
                                                array($propertyID => $propertyValue)
            );

        } else {
            echo 'error';
        }
    } else {
        echo 'error';
    }
    require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_after.php');