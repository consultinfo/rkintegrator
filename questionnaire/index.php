<?  require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
    $APPLICATION->SetTitle("Анкета на подключение к сервису");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.maskedinput.js');
    $UserLogin = $_GET['key'];
    $TypeApp = $_GET['app'];
    if($UserLogin != ''){
        global $USER;
        $c = new McryptCipher('KeYfOrPasS');
        if($UserLogin = $c->decrypt($UserLogin))
            $rsUser = CUser::GetByLogin($UserLogin);
        if($arUser = $rsUser->Fetch()){
            $USER->Authorize($arUser["ID"]);
        } else {
            LocalRedirect("/");
        }
    } else {
        LocalRedirect("/");
    }

    /* Поиск элемента, привязанного к текущему пользователю */
    CModule::IncludeModule("iblock");
    $elID = '';
    $arSelect = Array("ID", "IBLOCK_ID", "ACTIVE");
    $arFilter = Array("IBLOCK_ID" => ($TypeApp == 'llc') ? 2 : 3, "ACTIVE" => "",  "CREATED_USER_ID" => $USER->GetID());
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();

        if($arProps["APP_STATUS"]["VALUE_XML_ID"] == 'app_filling'){ //статус анкеты - заполняется
            $elFlagActive = 'Y';
        } else {
            $elFlagActive = 'N';
        }

        $elID = $arFields["ID"];

    }

    /* Если элемента нет, то создаем новый элемент, привязанный к текущему пользователю */
    if ($elID == '') {
        $el = new CIBlockElement;


        $settings = CIBlockElement::GetByID(123);
        $ob_settings = $settings->GetNextElement();
        $settings = $ob_settings->GetFields();
        $settings["PROPERTY"] = $ob_settings->GetProperties();

        $PROP = array();

        $arBlockLLC = array(
            'FLAG_1' => 40,
            'FLAG_2' => 41,
            'FLAG_3' => 42,
            'FLAG_4' => 43,
            'FLAG_5' => 44,
            'FLAG_6' => 45,
            'FLAG_7' => 46,
            'FLAG_8' => 47,
            'FLAG_9' => 48
        );
        $arBlockIE = array(
            'FLAG_1' => 49,
            'FLAG_2' => 50,
            'FLAG_3' => 51,
            'FLAG_4' => 52,
            'FLAG_5' => 53,
            'FLAG_6' => 54,
            'FLAG_7' => 55,
            'FLAG_8' => 56
        );

        if($TypeApp == 'llc') {
            $PROP[128] = 33; // ООО
            $PROP[6] = 24; // являюсь резидентом
            foreach($settings["PROPERTY"] as $nameProp => $arProp){
                if($arProp["VALUE"] != '' && substr($nameProp, 0, -2) == 'LLC_BLOCK'){
                    $nameProp = str_replace('LLC_BLOCK','FLAG',$nameProp);
                    $PROP[$nameProp] = $arBlockLLC[$nameProp];
                }
            }
        } else {
            $PROP[129] = 37;
            foreach ($settings["PROPERTY"] as $nameProp => $arProp) {
                if ($arProp["VALUE"] != '' && substr($nameProp, 0, -2) == 'IE_BLOCK') {
                    $nameProp = str_replace('IE_BLOCK', 'FLAG', $nameProp);
                    $PROP[ $nameProp ] = $arBlockIE[ $nameProp ];
                }
            }
        }

        $arLoadProductArray = Array(
            "MODIFIED_BY"       => $USER->GetID(),
            "CREATED_BY"        => $USER->GetID(),
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID"         => ($TypeApp == 'llc') ? 2 : 3,
            "NAME"              => $USER->GetLogin(),
            "ACTIVE"            => "Y",
            "PROPERTY_VALUES"   => $PROP,

        );
        $elID = $el->Add($arLoadProductArray);
        $elFlagActive = 'Y';
    }
    /* Если элемент есть и он активный, то разрешаем редактировать анкету, если нет (принята анкета), то показываем текст */?>
    <main class="workArea">
        <div class="container pt30">

    <div class="line clearFix relative">
        <h3><? $APPLICATION->ShowTitle(false); ?></h3>
    </div>
    </div>
    <?if ($elFlagActive == 'Y') {

        $IE_required = array(86,87,89,90,91,96,97,98,99,100,102,103,104,105,106,108,109,110,111,112,113,115,116,117,118,119,120,121,122,123,124,125,126,127,175);
        $LLC_required = array(1,4,5,7,133,8,9,10,76,77,78,79,11,12,13,14,15,16,17,18,19,20,21,22,24,25,34,37,38,39,74,41,43,47,55,56,57,58,52,168);


        $_REQUEST["CODE"] = $elID;
        $APPLICATION->IncludeComponent(
            "consultinfo:iblock.element.add.form",
            ($TypeApp == 'llc') ? "questionnaires_llc" : "questionnaires_ie",
            Array(
                "IBLOCK_TYPE"                   => "questionnaires",
                "IBLOCK_ID"                     => ($TypeApp == 'llc') ? 2 : 3,
                "STATUS_NEW"                    => "N",
                "LIST_URL"                      => "",
                "USE_CAPTCHA"                   => "N",
                "USER_MESSAGE_EDIT"             => "",
                "USER_MESSAGE_ADD"              => "",
                "DEFAULT_INPUT_SIZE"            => "",
                "RESIZE_IMAGES"                 => "N",
                "PROPERTY_CODES"                => array(),
                "PROPERTY_CODES_REQUIRED"       => ($TypeApp == 'llc') ? $LLC_required : $IE_required,
                "GROUPS"                        => array(),
                "STATUS"                        => "ANY",
                "ELEMENT_ASSOC"                 => "CREATED_BY",
                "MAX_USER_ENTRIES"              => "100000",
                "MAX_LEVELS"                    => "100000",
                "LEVEL_LAST"                    => "Y",
                "MAX_FILE_SIZE"                 => "0",
                "PREVIEW_TEXT_USE_HTML_EDITOR"  => "N",
                "DETAIL_TEXT_USE_HTML_EDITOR"   => "N",
                "SEF_MODE"                      => "N",
                "CUSTOM_TITLE_NAME"             => "",
                "CUSTOM_TITLE_TAGS"             => "",
                "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                "CUSTOM_TITLE_DATE_ACTIVE_TO"   => "",
                "CUSTOM_TITLE_IBLOCK_SECTION"   => "",
                "CUSTOM_TITLE_PREVIEW_TEXT"     => "",
                "CUSTOM_TITLE_PREVIEW_PICTURE"  => "",
                "CUSTOM_TITLE_DETAIL_TEXT"      => "",
                "CUSTOM_TITLE_DETAIL_PICTURE"   => "",
                /*"AJAX_MODE" => "Y",
                "AJAX_OPTION_JUMP" => "Y",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "Y",
                "SET_TITLE" => "N"*/
            )
        );?>
        <div id="address">
            <div class="closeWrap"><span class="b-close">&nbsp;</span></div>
            <header class="popup">
                <h4 class="center">Укажите адрес места жительства</h4>
            </header>
            <section class="popup">
                <h5 class="none">form</h5>
                <!-- <div class="notification failure">
                    <span class="ico failure">&nbsp;</span>
                    <span class="message">Заполните все обязательные поля!</span>
                </div> -->
                <div class="table">
                    <div class="tr col_chk" style="display: none;">
                        <div class="td value">
                            <label class="checkbox" for="id_31">
                                <span class="label">Совпадает с местом регистрации</span>
                                <input type="checkbox" id="id_31">
                                <span class="chkbutton">&nbsp;</span>
                            </label>
                        </div>
                    </div>
                    <div class="tr col_radio" style="display: none;">
                        <div class="td value">
                            <label class="radio" for="id_1">
                                <span class="label">Совпадает с местом регистрации</span>
                                <input name="sov" value="1" type="radio" id="id_1">
                                <span class="rbutton">&nbsp;</span>
                            </label>
                        </div>
                        <div class="td value">
                            <label class="radio" for="id_2">
                                <span class="label">Совпадает с местом фактического проживания</span>
                                <input name="sov" value="2" type="radio" id="id_2">
                                <span class="rbutton">&nbsp;</span>
                            </label>
                        </div>
                    </div>
                    <div class="tr col2">
                        <div class="td label"><span class="name">Индекс</span> </div>
                        <div class="td value"><input type="text" id="id_q"></div>
                        <div class="td label"><span class="name">Страна</span></div>
                        <div class="td value"><input type="text" id="id_w"></div>
                        <div class="td status"><!--<span class="saved">&nbsp;</span>--></div>
                    </div>
                    <div class="tr col1">
                        <div class="td label"><span class="name">Область/край</span></div>
                        <div class="td value"><input type="text" id="id_q"></div>
                        <div class="td status"></div>
                    </div>
                    <div class="tr col1">
                        <div class="td label"><span class="name">Район</span> </div>
                        <div class="td value"><input type="text" id="id_q"></div>
                        <div class="td status"></div>
                    </div>
                    <div class="tr col1">
                        <div class="td label"><span class="name">Город</span> </div>
                        <div class="td value"><input type="text" id="id_q"></div>
                        <div class="td status"></div>
                    </div>
                    <div class="tr col5">
                        <div class="td label"><span class="name">Дом</span> </div>
                        <div class="td value"><input type="text" id="id_q"></div>
                        <div class="td label"><span class="name">Корп.</span> </div>
                        <div class="td value"><input type="text" id="id_w"></div>
                        <div class="td label"><span class="name">Стр.</span> </div>
                        <div class="td value"><input type="text" id="id_e"></div>
                        <div class="td label"><span class="name">Кв.</span> </div>
                        <div class="td value"><input type="text" id="id_r"></div>
                        <div class="td label"><span class="name">Офис</span> </div>
                        <div class="td value"><input type="text" id="id_t"></div>
                        <div class="td status"></div>
                    </div>
                </div>
                <div class="center noselect">
                    <a class="btn b-close" href="javascript:void(0);">Добавить адрес</a>
                </div>
            </section>
        </div>

        <div id="phone">
            <div class="closeWrap"><span class="b-close">&nbsp;</span></div>
            <header class="popup">
                <h4 class="center">Укажите номер телефона</h4>
            </header>
            <section class="popup">
                <h5 class="none">form</h5>
                <form action="#">
                    <div class="row">
                        <div class="item">
                            <div class="label">Код страны</div>
                            <div class="value country">
                                <input type="text" id="country">
                            </div>
                        </div>
                        <div class="item">
                            <div class="label">Код города</div>
                            <div class="value city">
                                <input type="text" id="city">
                            </div>
                        </div>
                        <div class="item">
                            <div class="label">Номер телефона</div>
                            <div class="value number">
                                <input type="text" id="number">
                            </div>
                        </div>
                        <!--<div class="status">
                            <span class="saved">Сохранено</span>
                        </div>-->
                    </div>
                    <div class="center noselect">
                        <a class="btn b-close" href="javascript:void(0);">Добавить телефон</a>
                    </div>
                </form>
            </section>
        </div>



    <?} else {?>
        <div class="container light">
            <div class="static clearFix">
                <div class="app clearFix">
                    <div class="notification success">
                        <span class="ico success">&nbsp;</span>
                        <span class="message">Спасибо! Ваша анкета заполнена</span>
                    </div>
                </div>
            </div>
        </div>
        <?/*<div id="empty" style="display: block; width: 100%"></div>
        <script>
            var a = $(window).height() - $('header').height() - $('footer').height() - $('.light').height() ;
            $('#empty').css('height', a+'px');
        </script>*/?>
    <?}?>
    </main>

    <?require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>