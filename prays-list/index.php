<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Прайс-лист");
?><?
    if($_REQUEST["REGION_CODE"]) {
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "prays-list",
            Array(
                "ACTIVE_DATE_FORMAT" => "d F Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "3600",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "N",
                "COMPONENT_TEMPLATE" => "prays-list",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(0 => ""),
                "FILE_404" => "",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "16",
                "IBLOCK_TYPE" => "content",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "N",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "50",
                "PAGER_BASE_LINK" => "",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_PARAMS_NAME" => "arrPager",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_TITLE" => "",
                "PARENT_SECTION_CODE" => $_REQUEST["REGION_CODE"],
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(0 => "ROW_VALUE"),
                "SET_BROWSER_TITLE" => "Y",
                "SET_LAST_MODIFIED" => "Y",
                "SET_META_DESCRIPTION" => "Y",
                "SET_META_KEYWORDS" => "Y",
                "SET_STATUS_404" => "Y",
                "SET_TITLE" => "N",
                "SHOW_404" => "Y",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_BY2" => "SORT",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC"
            )
        ); ?><? $APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "regions",
            Array(
                "ADD_SECTIONS_CHAIN" => "Y",
                "CACHE_GROUPS" => "Y",
                "CACHE_NOTES" => "",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "COUNT_ELEMENTS" => "Y",
                "IBLOCK_ID" => "16",
                "IBLOCK_TYPE" => "content",
                "SECTION_CODE" => "",
                "SECTION_FIELDS" => "",
                "SECTION_ID" => "",
                "SECTION_URL" => "",
                "SECTION_USER_FIELDS" => "",
                "SHOW_PARENT_NAME" => "Y",
                "TOP_DEPTH" => "1",
                "VIEW_MODE" => ""
            )
        );
    }?>
        <?if(!$_REQUEST["REGION_CODE"]){?>
<div class="container pt30">
	<div class="line">
		<div class="fullWidth relative">
			<h3>
			Прайс-лист </h3>
			<div class="mm_location">
				<div class="mm_location__cell _label">
 <span class="value">Ваш регион:</span>
				</div>
				<div class="mm_location__cell _value">
 <a id="_mm_region_modal_link" href="javascript:void(0);" class="mm_location__link">Выберите регион</a>
				</div>
			</div>
		</div>
	</div>
</div>
 <script>
            $(document).ready(function(){
                $('#_mm_region_modal').bPopup({
                    speed: 250,
                    transition: 'slideUp'
                });
            });
        </script> <?}?>
<div id="subscribe">
	 <?CModule::IncludeModule("subscribe");

        $arOrder = Array("SORT"=>"ASC", "NAME"=>"ASC");
        $arFilter = Array("ACTIVE"=>"Y", "LID"=>LANG, "CODE" => $_REQUEST["REGION_CODE"]);
        $rsRubric = CRubric::GetList($arOrder, $arFilter);
        $arRubrics = array();
        while($arRubric = $rsRubric->GetNext())
        {
            $RUBRIC_ID = $arRubric["ID"];
        }
        $APPLICATION->IncludeComponent(
            "consultinfo:subscribe_price",
            "",
            Array(
                "RUB_ID" => $RUBRIC_ID,
                "AJAX_MODE" => "Y",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "Y",
                "SET_TITLE" => "N"
            )
        );?>
</div>
    <script>
        $('#_mm_region_modal_link').bind('click', function(e) {
            e.preventDefault();
            $('#_mm_region_modal').bPopup({
                speed: 250,
                transition: 'slideUp'
            });
        });
    </script><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>