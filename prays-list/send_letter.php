<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отправка рассылки");
?>
<div class="container pt30">
    <div class="line">
<?if($USER -> isAdmin()){
    CModule::IncludeModule("subscribe");
    if($_REQUEST["SENDING"] == "Y" && !empty($_REQUEST["RUBRIC_ID"])){
        $RUBRIC_ID = $_REQUEST["RUBRIC_ID"];
        $html = "<div id='mailsub' style='width:100%; padding:0;'>
        <div style='text-align: center;'>
            <!-- header -->
            <table cellpadding='0' cellspacing='0' style='width:800px; margin-top: 0; margin-right: auto; margin-bottom: 0; margin-left: auto; background: #e7eff3;'>
                <tbody>
                <tr>
                    <td>
                        <div style='text-align: center;'>
                            <table cellpadding='0' cellspacing='0' style='width:600px; margin-top: 0; margin-right: auto; margin-bottom: 0; margin-left: auto; border:0;'>
                                <tbody>
                                <tr>
                                    <td colspan='2' style='width:100%; height:30px'>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:50%'>
                                        <a href='http://www.fuelbroker.ru/' target='_blank' style='display: block; width:auto; text-decoration:none;'> <img width='196' src='http://www.fuelbroker.ru/include/subscribe/logo.png' height='40' border='0' alt='' style='display: block; width:auto; text-decoration:none; border:0;'> </a>
                                    </td>
                                    <td style='width:50%; text-align:right'>
                                        <img width='147' alt='phone' src='http://www.fuelbroker.ru/include/subscribe/tel.png' height='16' border='0' style='display: inline-block; width:auto; text-decoration:none; border:0;'>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan='2' style='width:100%; height:30px'>
                                        &nbsp;
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- title + 70 anniversary pic -->
            <table cellpadding='0' cellspacing='0' style='width:600px; margin-top: 0; margin-right: auto; margin-bottom: 0; margin-left: auto; color: #344e5a; font-family: Arial; border:0;'>
                <tbody>
                <tr>
                    <td style='width:100%; height:30px'>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style='width:100%;'>
                        <h1 style='font-size: 27px; font-family: Arial; font-weight: normal; margin:0;line-height:32px;'>Внесены изменения в Прайс-лист по региону '".$_REQUEST["REGION"]."'.Ознакомиться можно по ссылке.</h1>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- main text -->
            <table cellpadding='0' cellspacing='0' style='width:600px; margin:0 auto; color: #344e5a; font-family: Arial; text-align: justify; border:0;'>
                <tbody>
                <tr>
                    <td style='width:100%; height:20px'>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style='text-align:center;'>
                        <a href='".SITE_SERVER_NAME.$_REQUEST["REGION_URL"]."' target='_blank' style='display:inline-block; width: auto;'> <img alt='перейти на сайт' src='http://www.fuelbroker.ru/include/subscribe/button.jpg' style='display:block; max-width:100%; margin: 0; border:0;' width='180' height='50' border='0'> </a>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- features block -->
            <table cellpadding='0' cellspacing='0' style='width:800px; margin:0 auto; padding: 0; color: #344e5a; font-family: Arial; background: #c4d8e1; border:0;'>
                <tbody>
                <tr>
                    <td rowspan='5' style='width:30px;'>
                        &nbsp;
                    </td>
                    <td style='height:25px;'>
                        &nbsp;
                    </td>
                    <td rowspan='5' style='width:30px;'>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style='text-align: center; background: #c4d8e1;'>
                        <h2 style='font-size: 28px; margin: 0; font-weight: normal;'>Алгоритм Топливный Интегратор</h2>
                    </td>
                </tr>
                <tr>
                    <td style='height:10px;'>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style='text-align: center; background: #c4d8e1;'>
 <span style='display: block; width:100%; font-size: 16px; margin: 0 0; line-height:23px'>
				Брокерское обслуживание потребителей и поставщиков нефти, нефтепродуктов, химической продукции и природного газа на товарно-сырьевых биржах России </span>
                    </td>
                </tr>
                <tr>
                    <td style='height:25px;'>
                        &nbsp;
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- footer --> <!-- social network buttons --><!-- footer logotype -->
            <table cellpadding='0' cellspacing='0' style='width:600px; margin-top: 0; margin-right: auto; margin-bottom: 0; margin-left: auto; color: #344e5a; font-family: Arial; border:0;'>
                <tbody>
                <tr>
                    <td style='width:100%; height:20px'>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style='text-align:center;'>
                        <a href='https://www.facebook.com/fuelbroker' target='_blank' style='display:inline-block; width: 40px; height: 40px; margin: 0 10px 0 0; text-decoration:none; vertical-align: top;'> <img width='40' alt='fb' src='http://www.fuelbroker.ru/include/subscribe/fb.jpg' height='40' border='0' style='width:40px; height:40px; border:0; text-decoration:none;'> </a> <a href='http://vk.com/algorithm.toplivny.integrator' target='_blank' style='display:inline-block; width: 40px; height: 40px; margin: 0 10px 0 0; text-decoration:none; vertical-align: top;'> <img width='40' alt='vk' src='http://www.fuelbroker.ru/include/subscribe/vk.jpg' height='40' border='0' style='width:40px; height:40px; border:0; text-decoration:none;'> </a> <a href='http://www.linkedin.com/company/алгоритм-топливный-интегратор' target='_blank' style='display:inline-block; width: 40px; height: 40px; margin: 0 10px 0 0; text-decoration:none; vertical-align: top;'> <img width='40' alt='in' src='http://www.fuelbroker.ru/include/subscribe/in.jpg' height='40' border='0' style='width:40px; height:40px; border:0; text-decoration:none;'> </a> <a href='http://www.youtube.com/channel/UCOuTh0NfKc_9YWHnj78dj1w ' target='_blank' style='display:inline-block; width: 40px; height: 40px; margin: 0; text-decoration:none; vertical-align: top;'> <img width='40' alt='in' src='http://www.fuelbroker.ru/include/subscribe/youtube.jpg' height='40' border='0' style='width:40px; height:40px; border:0; text-decoration:none;'> </a>
                    </td>
                </tr>
                <tr>
                    <td style='width:100%; height:20px'>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style='width:100%; text-align:center;'>
                        <span style='display: block; width: 100%; margin:0 0 5px 0; font-size: 13px;'>Общество с ограниченной ответственностью «Алгоритм Топливный Интегратор»</span>
                    </td>
                </tr>
                <tr>
                    <td style='width:100%; text-align:center;'>
                        <span style='display: block; width: 100%; margin:0 0 5px 0; font-size: 13px;'>Адрес: 125167 Россия, Москва, Ленинградский проспект, 47/2 (БЦ «Авион»), подъезд 2, этаж 3.</span>
                    </td>
                </tr>
                <tr>
                    <td style='width:100%; text-align:center;'>
                        <span style='display: block; width: 100%; margin:0 0 5px 0; font-size: 13px;'>+7 (495) 780-97-00 | <a href='mailto:info@fuelbroker.ru' target='_blank' style='color: #344e5a; text-decoration: none'>info@fuelbroker.ru</a></span>
                    </td>
                </tr>
                <tr>
                    <td style='width:100%; height:5px'>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style='text-align:center;'>
                        <span style='display: block; width: 100%; margin:0 0 5px 0; font-size: 13px;'>Вы получили это письмо, т.к. являетесь клиентом компании «Алгоритм Топливный Интегратор»</span>
                    </td>
                </tr>
                <tr>
                    <td style='width:100%; text-align:center;'>
                        <span style='display: block; width: 100%; margin:0 0 5px 0; font-size: 13px;'> и/или подписались на рассылку на <a href='http://www.fuelbroker.ru/' target='_blank' style='color: #344e5a; text-decoration: underline'>fuelbroker.ru</a> или на <a href='http://www.fuelservice.ru/' target='_blank' style='color: #344e5a; text-decoration: underline'>fuelservice.ru</a></span>
                    </td>
                </tr>
                <tr>
                    <td style='width:100%; text-align:center;'>
                        <span style='display: block; width: 100%; margin:0 0 5px 0; font-size: 13px;'> <a href='http://www.fuelbroker.ru/unsubscribe.php?mid=#MAIL_ID#&mhash=#MAIL_MD5#' target='_blank' style='color: #344e5a; text-decoration: underline'>отписаться</a> </span>
                    </td>
                </tr>
                <tr>
                    <td style='width:100%; height:5px'>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style='text-align:center;'>
                        <span style='display: block; width: 100%; margin:0 0 5px 0; font-size: 13px; color: #781E32'>При перепечатке ссылка на компанию 'Алгоритм Топливный Интегратор' обязательна.</span>
                    </td>
                </tr>
                <tr>
                    <td style='width:100%; height:20px'>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style='text-align:center;'>
                        <a href='http://www.fuelbroker.ru/' target='_blank' style='display: inline-block; width:auto; text-decoration:none;'> <img width='100' alt='logotype' src='http://www.fuelbroker.ru/include/subscribe/f_logo.jpg' height='51' border='0' style='display: block; width:auto; text-decoration:none; border:0;'> </a>
                    </td>
                </tr>
                <tr>
                    <td style='width:100%; height:20px'>
                        &nbsp;
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>";
        $strEmail = COption::GetOptionString('main','email_from');
        $new_date = $DB->FormatDate(date("d.m.Y H:i:s"), "DD.MM.YYYY HH:MI:SS", CSite::GetDateFormat("FULL", "ru"));
        $arFields = Array(
            "SUBJECT" => "Прайс-лист",
            "BODY_TYPE" => "html",
            "BODY" => $html,
            "DIRECT_SEND" => "Y",
            "CHARSET" => "UTF-8",
            "SUBSCR_FORMAT" => "html",
            "RUB_ID" => array($RUBRIC_ID),
            "FROM_FIELD" => $strEmail
            //"AUTO_SEND_TIME" => $new_date
        );
        $cPosting = new CPosting;
        $ID = $cPosting->Add($arFields);
        if($ID)
        {
            $cPosting->ChangeStatus($ID, "P");
            CAgent::AddAgent("CPosting::AutoSend(".$ID.",true);", "subscribe", "N", 0, $new_date, "Y", $new_date);
            //$cPosting->AutoSend($ID);
        }

    }
}
if($ID)
{?>
    <h2>Рассылка успешно отправлена № <?=$ID?></h2>
    <a href="/prays-list/">Вернуться к спискку</a>
<?} else {?>
    <h2><?=$cPosting -> LAST_ERROR?></h2>
        <?}?>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
