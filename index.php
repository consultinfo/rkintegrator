<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle("Покупка и продажа нефтепродуктов ведущих российских производителей");?>
    <section class="banner noselect">
        <h5 class="none"><?= GetMessage("MAIN_BANNER") ?></h5>
        <div class="zoom">&nbsp;</div>
        <div class="container">
            <div class="line">
                <section class="whatIs">
                    <h5 class="none">Список</h5>
                    <article class="inner">
                        <header class="algorithm">
                            <h4>
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/main/banner/whatis/anew.php"), false); ?>

                        </h4>
                        </header>
                        </article>
                </section>
                <section class="bannerContent">
                    <h2 class="alt">
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/main/banner/content/title.php"), false); ?>
                    </h2>
                    <div class="temporary">
                        <span class="value"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/main/banner/content/a0.php"), false); ?></span>
                    </div>
                    <div class="delivery">
                        <span class="block"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/main/banner/content/title2.php"), false); ?></span>

                        <div class="item">
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/main/banner/content/a2.php"), false); ?>
                        </div>
                        <div class="item">
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/main/banner/content/a3.php"), false); ?>
                        </div>
                        <div class="more">
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/main/banner/content/more.php"), false); ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
    <section class="about">
        <h5 class="none"><?= GetMessage("MAIN_WHAT_IS_STOCK") ?></h5>
        <div class="container">
            <div class="line">
            </div>
            <div class="line featuresWrapper">
                <div class="oneFourth animate_afl d3">
                    <section class="inner">
                        <header class="feature">
                            <h4><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/main/about/a1_title.php"), false); ?></h4>
                        </header>
                    </section>
                </div>
                <div class="oneFourth animate_afl d1">
                    <section class="inner">
                        <header class="feature">
                            <h4><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/main/about/a2_title.php"), false); ?></h4>
                        </header>

                    </section>
                </div>
                <div class="divide clearFix">&nbsp;</div>
                <div class="oneFourth animate_afr d1">
                    <section class="inner">
                        <header class="feature">
                            <h4><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/main/about/a3_title.php"), false); ?></h4>
                        </header>
                    </section>
                </div>
                <div class="oneFourth animate_afr d3">
                    <section class="inner">
                        <header class="feature">
                            <h4><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/main/about/a4_title.php"), false); ?></h4>
                        </header>
                    </section>
                </div>
            </div>

        </div>
    </section>
<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "main_services",
    Array(
        "IBLOCK_TYPE" => "content",
        "IBLOCK_ID" => "4",
        "NEWS_COUNT" => "",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "ACTIVE_FROM",
        "SORT_ORDER2" => "DESC",
        "FILTER_NAME" => "Filter_Services",
        "FIELD_CODE" => array("", "", ""),
        "PROPERTY_CODE" => array("SERVICES_CLASS", "PREVIEW_TEXT_MAIN", ""),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "N",
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "PAGER_TEMPLATE" => ".default",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N"
    )
);?>

<?
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/tabby.css');
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/tabby.js');
    require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>