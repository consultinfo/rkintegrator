<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Пользовательское соглашение");
?><style>
    <!--
    /* Font Definitions */
    @font-face
    {font-family:Wingdings;
        panose-1:5 0 0 0 0 0 0 0 0 0;}
    @font-face
    {font-family:Wingdings;
        panose-1:5 0 0 0 0 0 0 0 0 0;}
    @font-face
    {font-family:Cambria;
        panose-1:2 4 5 3 5 4 6 3 2 4;}
    @font-face
    {font-family:Calibri;
        panose-1:2 15 5 2 2 2 4 3 2 4;}
    @font-face
    {font-family:Tahoma;
        panose-1:2 11 6 4 3 5 4 4 2 4;}
    /* Style Definitions */
    p.MsoNormal, li.MsoNormal, div.MsoNormal
    {margin-top:0cm;
        margin-right:0cm;
        margin-bottom:10.0pt;
        margin-left:0cm;
        line-height:115%;
        font-size:12.0pt;
        font-family:"Times New Roman","serif";}

    p.MsoCommentText, li.MsoCommentText, div.MsoCommentText
    {mso-style-link:"Текст примечания Знак";
        margin-top:0cm;
        margin-right:0cm;
        margin-bottom:10.0pt;
        margin-left:0cm;
        font-size:10.0pt;
        font-family:"Times New Roman","serif";}
    p.MsoHeader, li.MsoHeader, div.MsoHeader
    {mso-style-link:"Верхний колонтитул Знак";
        margin:0cm;
        margin-bottom:.0001pt;
        font-size:12.0pt;
        font-family:"Times New Roman","serif";}
    p.MsoFooter, li.MsoFooter, div.MsoFooter
    {mso-style-link:"Нижний колонтитул Знак";
        margin:0cm;
        margin-bottom:.0001pt;
        font-size:12.0pt;
        font-family:"Times New Roman","serif";}
    a:link, span.MsoHyperlink
    {color:blue;
        text-decoration:underline;}
    a:visited, span.MsoHyperlinkFollowed
    {color:purple;
        text-decoration:underline;}
    p
    {margin-right:0cm;
        margin-left:0cm;
        font-size:10.0pt;
        font-family:"Arial","sans-serif";}
    p.MsoCommentSubject, li.MsoCommentSubject, div.MsoCommentSubject
    {mso-style-link:"Тема примечания Знак";
        margin-top:0cm;
        margin-right:0cm;
        margin-bottom:10.0pt;
        margin-left:0cm;
        font-size:10.0pt;
        font-family:"Times New Roman","serif";
        font-weight:bold;}
    p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
    {mso-style-link:"Текст выноски Знак";
        margin:0cm;
        margin-bottom:.0001pt;
        font-size:8.0pt;
        font-family:"Tahoma","sans-serif";}
    p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
    {margin-top:0cm;
        margin-right:0cm;
        margin-bottom:0cm;
        margin-left:36.0pt;
        margin-bottom:.0001pt;
        font-size:12.0pt;
        font-family:"Arial","sans-serif";}
    p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
    {margin-top:0cm;
        margin-right:0cm;
        margin-bottom:0cm;
        margin-left:36.0pt;
        margin-bottom:.0001pt;
        font-size:12.0pt;
        font-family:"Arial","sans-serif";}
    p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
    {margin-top:0cm;
        margin-right:0cm;
        margin-bottom:0cm;
        margin-left:36.0pt;
        margin-bottom:.0001pt;
        font-size:12.0pt;
        font-family:"Arial","sans-serif";}
    p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
    {margin-top:0cm;
        margin-right:0cm;
        margin-bottom:0cm;
        margin-left:36.0pt;
        margin-bottom:.0001pt;
        font-size:12.0pt;
        font-family:"Arial","sans-serif";}
    span.3
    {mso-style-name:"Заголовок 3 Знак";
        mso-style-link:"Заголовок 3";
        font-family:"Cambria","serif";
        color:#4F81BD;
        font-weight:bold;}
    span.a
    {mso-style-name:"Текст примечания Знак";
        mso-style-link:"Текст примечания";
        font-family:"Times New Roman","serif";}
    span.a0
    {mso-style-name:"Текст выноски Знак";
        mso-style-link:"Текст выноски";
        font-family:"Tahoma","sans-serif";}
    span.a1
    {mso-style-name:"Тема примечания Знак";
        mso-style-link:"Тема примечания";
        font-family:"Times New Roman","serif";
        font-weight:bold;}
    span.a2
    {mso-style-name:"Верхний колонтитул Знак";
        mso-style-link:"Верхний колонтитул";
        font-family:"Times New Roman","serif";}
    span.a3
    {mso-style-name:"Нижний колонтитул Знак";
        mso-style-link:"Нижний колонтитул";
        font-family:"Times New Roman","serif";}
    /* Page Definitions */
    @page WordSection1
    {size:595.3pt 841.9pt;
        margin:2.0cm 42.5pt 2.0cm 3.0cm;}
    div.WordSection1
    {page:WordSection1;}
    /* List Definitions */
    ol
    {margin-bottom:0cm;}
    ul
    {margin-bottom:0cm;}
    -->
</style>
<div class="WordSection1 line">
 <section class="clearFix">
	<p class="MsoNormal" align="center" style="margin-top:6.0pt;margin-right:0cm; margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:center; line-height:normal">
 <b>СОГЛАСИЕ<br>
		 посетителя сайта на обработку персональных данных</b>
	</p>
	<p class="MsoNormal" style="margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm; margin-left:0cm;margin-bottom:.0001pt;line-height:normal">
		 &nbsp;
	</p>
	<p class="MsoNormal" style="margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm; margin-left:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal">
		 Настоящим свободно, своей волей и в своем интересе даю согласие Акционерному обществу «РК Интегратор», распологающемуся по адресу: 295011, Республика Крым, город Симферополь, ул. Жуковского, дом 25, помещение 27, комната 204 (далее – АО «РКИ»), на автоматизированную и неавтоматизированную обработку моих персональных данных, в том числе с использованием интернет-сервисов <span lang="EN-US">G</span>oogle analytics, Яндекс.Метрика, LiveInternet, Рейтинг Mail.ru, Google Doubleclick в соответствии со следующим перечнем:
	</p>
	<p class="MsoNormal" style="margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm; margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal">
 <span style="font-family:Symbol">-<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>источник захода на сайт <a href="http://rkintegrator.ru">http://rkintegrator.ru</a> (далее – Сайт) и информация поискового или рекламного запроса;
	</p>
	<p class="MsoNormal" style="margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm; margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal">
 <span style="font-family:Symbol">-<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>фамилия, имя, номер телефона и иные данные, введенные мной в формы на Сайте;
	</p>
	<p class="MsoNormal" style="margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm; margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal">
 <span style="font-family:Symbol">-<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>данные о пользовательском устройстве (среди которых разрешение, версия и другие атрибуты, характеризующие пользовательское устройство);
	</p>
	<p class="MsoNormal" style="margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm; margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal">
 <span style="font-family:Symbol">-<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>пользовательские клики, просмотры страниц, заполнения полей, показы и просмотры баннеров и видео;
	</p>
	<p class="MsoNormal" style="margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm; margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal">
 <span style="font-family:Symbol">-<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>данные, характеризующие аудиторные сегменты;
	</p>
	<p class="MsoNormal" style="margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm; margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal">
 <span style="font-family:Symbol">-<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>параметры сессии;
	</p>
	<p class="MsoNormal" style="margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm; margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal">
 <span style="font-family:Symbol">-<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>данные о времени посещения;
	</p>
	<p class="MsoNormal" style="margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm; margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal">
 <span style="font-family:Symbol">-<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>идентификатор пользователя, хранимый в cookie,
	</p>
	<p class="MsoNormal" style="margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm; margin-left:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal">
		 для повышения осведомленности посетителей Сайта&nbsp; о продуктах и услугах АО «РКИ», предоставления релевантной рекламной информации и оптимизации рекламы.
	</p>
	<p class="MsoNormal" style="margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm; margin-left:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal">
		 Также даю свое согласие на предоставление АО «РКИ» моих персональных данных как посетителя Сайта агентствам, с которыми сотрудничает АО «РКИ». АО «РКИ» вправе осуществлять обработку моих персональных данных следующими способами: сбор, запись, систематизация, накопление, хранение, обновление, изменение, использование, передача (распространение, предоставление, доступ).
	</p>
	<p class="MsoNormal" style="margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm; margin-left:0cm;margin-bottom:.0001pt;line-height:normal">
		 Настоящее согласие вступает в силу с момента моего перехода на Сайт и действует в течение сроков, установленных действующим законодательством РФ.
	</p>
	<p class="MsoNormal" style="margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm; margin-left:0cm;margin-bottom:.0001pt;line-height:normal">
		 &nbsp;
	</p>
 </section>
</div>


<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php"); ?>