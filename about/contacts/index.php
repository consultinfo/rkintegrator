<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
    $APPLICATION->SetTitle("Как нас найти"); ?>
    <div class="line">
    <section class="common clearFix">
        <h5 class="none">common seсtion content</h5>
        <div class="contactsWrapper clearFix">
            <div class="item">
                <p>
                    <span class="block">
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/address.php"), false); ?>
                    </span>
                </p>
                <div class="getTheRoute">
                    <a target="_blank" href="https://goo.gl/VsJGXl">
                        <span class="ico info"></span>
                        <span class="value">Google Карты</span>
                    </a>
                </div>
                <div class="getTheRoute">
                    <a target="_blank" href="https://clck.ru/9b7cn">
                        <span class="ico info"></span>
                        <span class="value">Яндекс.Карты</span>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="phone">
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/phone.php"), false); ?>
                </div>
                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/contacts_email.php"), false); ?>
            </div>

        </div>

        <?/*$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "regional_offices",
            Array(
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => "15",
                "NEWS_COUNT" => "",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "ACTIVE_FROM",
                "SORT_ORDER2" => "DESC",
                "FILTER_NAME" => "",
                "FIELD_CODE" => array("", "", ""),
                "PROPERTY_CODE" => array("ADDRESS", ""),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "N",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N"
            )
        );*/?>
    </section>
        </div>
    <section class="map">
        <h6 class="none">here is our location</h6>
        <div id="location">
            <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view", 
	".default", 
	array(
		"CONTROLS" => array(
		),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:44.94869178673721;s:10:\"yandex_lon\";d:34.09180111045109;s:12:\"yandex_scale\";i:17;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:34.09172064418;s:3:\"LAT\";d:44.94859083741;s:4:\"TEXT\";s:161:\"Республика Крым,###RN###город Симферополь, ул. Жуковского,###RN###дом 25, помещение 27, комната 204 \";}}}",
		"MAP_HEIGHT" => "600",
		"MAP_ID" => "",
		"MAP_WIDTH" => "100%",
		"OPTIONS" => array(
			0 => "ENABLE_SCROLL_ZOOM",
		),
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
            <?/*?><a
                class="dg-widget-link"
                href="http://2gis.ru/moscow/profiles/4504127908768397,70000001019617798/center/37.53191471099854,55.79865167072753/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">
                Посмотреть на карте Москвы
            </a>
            <script charset="utf-8" src="http://widgets.2gis.com/js/DGWidgetLoader.js"></script>
            <script charset="utf-8">
                new DGWidgetLoader({
                    "width":640,
                    "height":600,
                    "borderColor":"#a3a3a3",
                    "pos":{
                        "lat":55.79865167072753,
                        "lon":37.53191471099854,
                        "zoom":16
                    },
                    "opt":{
                        "city":"moscow"
                    },
                    "org":[{"id":"4504127908768397"},{"id":"70000001019617798"}]
                });
            </script>
            <noscript style="color:#c00;font-size:16px;font-weight:bold;">
                Виджет карты использует JavaScript. Включите его в настройках вашего браузера.
            </noscript>
            <?*/?>
        </div>
    </section>


    <script>
        $(window).load(function () {
            //alert('!');
            //$('.leaflet-popup-close-button').click();

            console.log($('#location').find('iframe').find('.leaflet-map-pane'));
        });
        $('.map iframe').ready(function(){

        })
    </script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>