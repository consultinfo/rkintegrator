<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Один из ключевых независимых игроков сырьевого и товарного рынков, обеспечивающих крупнооптовые и мелкооптовые поставки топлива в адрес конечных потребителей Республики Крым");
$APPLICATION->SetPageProperty("keywords", "Республика Крым, мелкооптовая реализация, поставки топлива");
$APPLICATION->SetPageProperty("title", "Компания «РК Интегратор» | Информация о компании");
    $APPLICATION->SetTitle("О компании"); ?>
    <div class="container">
        <div class="line">
            <section class="common clearFix" style="padding: 0;">
                <h5 class="none">common setion content</h5>
                <!--<figure class="common">
                    <?/* $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/img.php"), false); */?>
                </figure>-->
                <section class="text">
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/content.php"), false); ?>
                </section>
            </section>
        </div>
    </div>
    <div class="container advatages">
        <div class="line">
            <div class="fullWidth clearFix">
                <h2 class="center"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/fullWidth_block_title.php"), false); ?></h2>
                <article class="advantage">
                    <h4><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/article1_title.php"), false); ?></h4>
                    <section class="desc">
                        <h5 class="none">advatage description</h5>
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/article1.php"), false); ?>
                    </section>
                </article>
                <article class="advantage">
                    <h4><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/article2_title.php"), false); ?></h4>
                    <section class="desc">
                        <h5 class="none">advatage description</h5>
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/article2.php"), false); ?>
                    </section>
                </article>
                <div class="divide">&nbsp;</div>
                <article class="advantage">
                    <h4><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/article3_title.php"), false); ?></h4>
                    <section class="desc">
                        <h5 class="none">advatage description</h5>
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/article3.php"), false); ?>
                    </section>
                </article>
            </div>
        </div>
    </div>
<? $APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "about_team",
    Array(
        "IBLOCK_TYPE"                     => "content",
        "IBLOCK_ID"                       => "9",
        "NEWS_COUNT"                      => "",
        "SORT_BY1"                        => "SORT",
        "SORT_ORDER1"                     => "ASC",
        "SORT_BY2"                        => "ACTIVE_FROM",
        "SORT_ORDER2"                     => "DESC",
        "FILTER_NAME"                     => "",
        "FIELD_CODE"                      => array("", "", ""),
        "PROPERTY_CODE"                   => array("SERVICES_CLASS", "", ""),
        "CHECK_DATES"                     => "Y",
        "DETAIL_URL"                      => "",
        "AJAX_MODE"                       => "N",
        "AJAX_OPTION_JUMP"                => "N",
        "AJAX_OPTION_STYLE"               => "Y",
        "AJAX_OPTION_HISTORY"             => "N",
        "CACHE_TYPE"                      => "A",
        "CACHE_TIME"                      => "36000000",
        "CACHE_FILTER"                    => "N",
        "CACHE_GROUPS"                    => "Y",
        "PREVIEW_TRUNCATE_LEN"            => "",
        "ACTIVE_DATE_FORMAT"              => "d.m.Y",
        "SET_TITLE"                       => "N",
        "SET_BROWSER_TITLE"               => "N",
        "SET_META_KEYWORDS"               => "N",
        "SET_META_DESCRIPTION"            => "N",
        "SET_STATUS_404"                  => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
        "ADD_SECTIONS_CHAIN"              => "N",
        "HIDE_LINK_WHEN_NO_DETAIL"        => "N",
        "PARENT_SECTION"                  => "",
        "PARENT_SECTION_CODE"             => "",
        "INCLUDE_SUBSECTIONS"             => "N",
        "DISPLAY_DATE"                    => "N",
        "DISPLAY_NAME"                    => "Y",
        "DISPLAY_PICTURE"                 => "N",
        "DISPLAY_PREVIEW_TEXT"            => "Y",
        "PAGER_TEMPLATE"                  => ".default",
        "DISPLAY_TOP_PAGER"               => "N",
        "DISPLAY_BOTTOM_PAGER"            => "N",
        "PAGER_TITLE"                     => "",
        "PAGER_SHOW_ALWAYS"               => "N",
        "PAGER_DESC_NUMBERING"            => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL"                  => "N"
    )
); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>