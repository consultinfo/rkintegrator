<p align="justify">
	 <a href="/upload/Торговая политика РКИ.pdf">Торговая политика АО «РК Интегратор»&nbsp;в области мелкооптовой реализации автомобильных бензинов и дизельного топлива на территории Республики Крым Российской Федерации</a>
</p>
<p align="justify">
	 Компания «РК Интегратор» (АО «РК Интегратор») – один из ключевых независимых игроков сырьевого и товарного рынков, обеспечивающих крупнооптовые и мелкооптовые поставки топлива в адрес конечных потребителей Республики Крым. Компания самостоятельно осуществляет мелкооптовую реализацию на территории Республики с нефтебаз независимых участников рынка.
</p>
<p align="justify">
	 Приоритет АО «РК Интегратор» - поставки качественной продукции (ГОСТ) с НПЗ крупнейших российских ВИНКов.
</p>
<p align="justify">
	 Ценовая политика компании в Республике Крым является абсолютно прозрачной и обеспечивает недискриминационный доступ к ресурсам любого заинтересованного потребителя.
</p>
<p align="justify">
 <b>6 профессиональных принципов Компании:</b>
</p>
<ul style="text-align: justify;">
	<li>
	<p align="justify">
		 Нацелены на долгосрочные взаимовыгодные отношения с клиентами.
	</p>
 </li>
	<li>
	<p align="justify">
		 Занимаем активную антикоррупционную позицию.
	</p>
 </li>
	<li>
	<p align="justify">
		 Проверяем репутацию и добросовестность контрагентов.
	</p>
 </li>
	<li>
	<p align="justify">
		 Участвуем только в 100% легальных сделках.
	</p>
 </li>
	<li>
	<p align="justify">
		 В основе нашего успеха лежат оптимально выстроенные бизнес-процессы и четкое следование правилам и процедурам биржевой торговли.
	</p>
 </li>
	<li>
	<p align="justify">
		 В основе долгосрочного преуспевания компании — ее талантливые и целеустремленные сотрудники.
	</p>
 </li>
</ul>
<p>
</p>
<ul style="text-align: justify;">
	<blockquote class="boxed">
	</blockquote>
</ul>