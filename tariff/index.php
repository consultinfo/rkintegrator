<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Брокерское обслуживание на рынке нефти, нефтепродуктов, природного газа и других товаров Российской товарно-сырьевой биржи. Агентирование логистики товаров по России.");
$APPLICATION->SetPageProperty("keywords", "брокер природный газ, брокер нефтепродуктов, брокер газ, товарно сырьевая биржа в России, Российская товарно сырьевая биржа, Российская товарная биржа, товарный брокер, топливо на бирже, товарная биржа в России, товарный брокер");
$APPLICATION->SetPageProperty("title", "Тарифы — Брокер газа, топлива, нефти и нефтепродуктов | Логистика по России");
    $APPLICATION->SetTitle("Тарифы — Брокер газа, топлива, нефти и нефтепродуктов | Логистика по России"); ?>
    <!--<div class="container type light">
        <div class="line">
            <h4>Тарифный план "Базовый"<sup>1</sup></h4>
        </div>
    </div>-->
    <div class="container">
        <div class="line">
            <section class="common">
                <h5 class="none">trafiff list</h5>
                <article class="tariff">
                    <h5 class="border bold"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/title.php"), false); ?></h5>
                    <div class="unit darker">
                        <div class="label"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr1td1.php"), false); ?></div>
                        <div class="value header border">
                            <div class="ilable"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr1td1.php"), false); ?></div>
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr1td2.php"), false); ?>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td2.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td2.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td2.php"), false); ?></span>
                            </div>
                        </div>
                        <div class="value header border">
                            <div class="ilable"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr1td1.php"), false); ?></div>
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr1td3.php"), false); ?>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td3.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td3.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td3.php"), false); ?></span>
                            </div>
                        </div>
                        <div class="value header border">
                            <div class="ilable"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr1td1.php"), false); ?></div>
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr1td4.php"), false); ?>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td4.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td4.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td4.php"), false); ?></span>
                            </div>
                        </div>
                        <div class="value header">
                            <div class="ilable"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr1td1.php"), false); ?></div>
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr1td5.php"), false); ?>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td5.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td5.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td5.php"), false); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="unit dark">
                        <div class="label"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td1.php"), false); ?></div>
                        <div class="value border"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td2.php"), false); ?></div>
                        <div class="value border"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td3.php"), false); ?></div>
                        <div class="value border"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td4.php"), false); ?></div>
                        <div class="value"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td5.php"), false); ?></div>
                    </div>
                    <div class="unit light">
                        <div class="label"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td1.php"), false); ?></div>
                        <div class="value border"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td2.php"), false); ?></div>
                        <div class="value border"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td3.php"), false); ?></div>
                        <div class="value border"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td4.php"), false); ?></div>
                        <div class="value"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td5.php"), false); ?></div>
                    </div>
                    <div class="unit lighter">
                        <div class="label"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td1.php"), false); ?></div>
                        <div class="value border"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td2.php"), false); ?></div>
                        <div class="value border"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td3.php"), false); ?></div>
                        <div class="value border"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td4.php"), false); ?></div>
                        <div class="value"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td5.php"), false); ?></div>
                    </div>
                </article>
                <article class="tariff">
                    <h5 class="border bold"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/title.php"), false); ?></h5>
                    <div class="unit darker">
                        <div class="label"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr1td1.php"), false); ?></div>
                        <div class="value header border">
                            <div class="ilable"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr1td1.php"), false); ?></div>
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr1td2.php"), false); ?>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2td2.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr3td1.php"), false); ?>
                                <span class="ivalue">100</span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4td2.php"), false); ?></span>
                            </div>
                        </div>
                        <div class="value header border">
                            <div class="ilable"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr1td1.php"), false); ?></div>
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr1td3.php"), false); ?>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2td3.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr3td1.php"), false); ?>
                                <span class="ivalue">50</span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4td3.php"), false); ?></span>
                            </div>
                        </div>
                        <div class="value header border">
                            <div class="ilable"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr1td1.php"), false); ?></div>
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr1td4.php"), false); ?>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2td4.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr3td1.php"), false); ?>
                                <span class="ivalue">50</span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4td4.php"), false); ?></span>
                            </div>
                        </div>
                        <div class="value header">
                            <div class="ilable"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr1td1.php"), false); ?></div>
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr1td5.php"), false); ?>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2td5.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr3td1.php"), false); ?>
                                <span class="ivalue">50</span>
                            </div>
                            <div class="ilable">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4td1.php"), false); ?>
                                <span class="ivalue"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4td5.php"), false); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="unit dark">
                        <div class="label"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2td1.php"), false); ?></div>
                        <!-- <div class="value full">В рублях за тонну</div> -->
                        <div class="value border"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2td2.php"), false); ?></div>
                        <div class="value border"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2td3.php"), false); ?></div>
                        <div class="value border"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2td4.php"), false); ?></div>
                        <div class="value"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2td5.php"), false); ?></div>
                    </div>
                    <div class="unit light">
                        <div class="label"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr3td1.php"), false); ?></div>
                        <div class="value border">100</div>
                        <div class="value border">50</div>
                        <div class="value border">50</div>
                        <div class="value">50</div>
                    </div>
                    <div class="unit lighter">
                        <div class="label"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4td1.php"), false); ?></div>
                        <!-- <div class="value full">Не включены в комиссию брокера</div> -->
                        <div class="value border"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4td2.php"), false); ?></div>
                        <div class="value border"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4td3.php"), false); ?></div>
                        <div class="value border"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4td4.php"), false); ?></div>
                        <div class="value"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4td5.php"), false); ?></div>
                    </div>
                </article>
            </section>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>