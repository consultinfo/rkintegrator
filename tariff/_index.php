<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
    $APPLICATION->SetTitle("Тарифы"); ?>
    <!--<div class="container type light">
        <div class="line">
            <h4>Тарифный план "Базовый"</h4>
        </div>
    </div>-->
    <div class="container">
        <div class="line">
            <section class="common">
                <h5 class="none">trafiff list</h5>
                <article class="tariff">
                    <h5 class="border bold"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/title.php"), false); ?></h5>
                    <div class="unit darker">
                        <div class="label"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr1_title.php"), false); ?></div>
                        <div class="valueHalf header border">
                            <div class="ilable"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr1_title.php"), false); ?></div>
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr1td1.php"), false); ?>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td1.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td1.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td1.php"), false); ?></span>
                            </div>
                        </div>
                        <div class="valueHalf header">
                            <div class="ilable"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr1_title.php"), false); ?></div>
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr1td2.php"), false); ?>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td2.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td2.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td2.php"), false); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="unit dark">
                        <div class="label"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2_title.php"), false); ?></div>
                        <div class="valueHalf border"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td1.php"), false); ?></div>
                        <div class="valueHalf"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr2td2.php"), false); ?></div>
                    </div>
                    <div class="unit light">
                        <div class="label"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3_title.php"), false); ?></div>
                        <div class="valueHalf border"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td1.php"), false); ?></div>
                        <div class="valueHalf"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr3td2.php"), false); ?></div>
                    </div>
                    <div class="unit lighter">
                        <div class="label"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4_title.php"), false); ?></div>
                        <div class="valueHalf border"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td1.php"), false); ?></div>
                        <div class="valueHalf"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table1/tr4td2.php"), false); ?></div>
                    </div>
                </article>
                <article class="tariff">
                    <h5 class="border bold"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table3/title.php"), false); ?></h5>
                    <div class="item header clearFix">
                        <div class="label"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table3/tr1_title.php"), false); ?></div>
                        <div class="value header darker">
                            <span class="upp"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table3/tr1.php"), false); ?></span>
                        </div>
                    </div>
                    <div class="item clearFix">
                        <div class="label"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table3/tr2_title.php"), false); ?></div>
                        <div class="value dark"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table3/tr2.php"), false); ?></div>
                    </div>
                    <div class="item clearFix">
                        <div class="label"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table3/tr3_title.php"), false); ?></div>
                        <div class="value light"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table3/tr3.php"), false); ?></div>
                    </div>
                    <div class="item clearFix">
                        <div class="label"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table3/tr4_title.php"), false); ?></div>
                        <div class="value lighter"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table3/tr4.php"), false); ?></div>
                    </div>
                </article>
                <article class="tariff">
                    <h5 class="border bold"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/title.php"), false); ?></h5>
                    <div class="unit darker">
                        <div class="label"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr1_title.php"), false); ?></div>
                        <div class="value header border">
                            <div class="ilable"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr1_title.php"), false); ?></div>
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/td1_title.php"), false); ?>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr3_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/td1.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4.php"), false); ?></span>
                            </div>
                        </div>
                        <div class="value header border">
                            <div class="ilable"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr1_title.php"), false); ?></div>
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/td2_title.php"), false); ?>
                             <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr3_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/td2.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4.php"), false); ?></span>
                            </div>
                        </div>
                        <div class="value header border">
                            <div class="ilable"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr1_title.php"), false); ?></div>
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/td3_title.php"), false); ?>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr3_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/td3.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4.php"), false); ?></span>
                            </div>
                        </div>
                        <div class="value header">
                            <div class="ilable"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr1_title.php"), false); ?></div>
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/td4_title.php"), false); ?>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr3_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/td4.php"), false); ?></span>
                            </div>
                            <div class="ilable">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4_title.php"), false); ?>
                                <span class="ivalue"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4.php"), false); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="unit dark">
                        <div class="label"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2_title.php"), false); ?></div>
                        <div class="value border"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2.php"), false); ?></div>
                        <div class="value border"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2.php"), false); ?></div>
                        <div class="value border"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2.php"), false); ?></div>
                        <div class="value"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr2.php"), false); ?></div>
                    </div>
                    <div class="unit light">
                        <div class="label"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr3_title.php"), false); ?></div>
                        <div class="value border"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/td1.php"), false); ?></div>
                        <div class="value border"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/td2.php"), false); ?></div>
                        <div class="value border"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/td3.php"), false); ?></div>
                        <div class="value"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/td4.php"), false); ?></div>
                    </div>
                    <div class="unit lighter">
                        <div class="label"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4_title.php"), false); ?></div>
                        <div class="value border"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4.php"), false); ?></div>
                        <div class="value border"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4.php"), false); ?></div>
                        <div class="value border"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4.php"), false); ?></div>
                        <div class="value"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "include/table2/tr4.php"), false); ?></div>
                    </div>
                </article>
                <span>Все тарифы указаны с учетом НДС</span>
            </section>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>