<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("404 Not Found");?>

    <section class="notfound">
        <div class="container">
            <div class="line">
                <section class="fail clearFix">
                    <h5 class="none">sorry, the page not found</h5>
                    <section class="content">
                        <h1>404</h1>
                        <h3>К сожалению, запрашиваемая вами страница не найдена</h3>
                        <div class="subtitle">
                            Возможно, страница была удалена, или вы неправильно набрали адрес
                        </div>
                        <a class="goto" href="/">Перейти на главную страницу</a>
                    </section>
                    <figure class="pic">
                        <img src="/bitrix/templates/consultinfo/images/content/404/no.jpg" alt="" class="responsiveAlt">
                    </figure>
                </section>
            </div>
        </div>
    </section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>